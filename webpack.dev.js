import webpack from 'webpack';
import WebpackNotifierPlugin from 'webpack-notifier';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import path from 'path';
import base from './webpack.base';


const config = {
  ...base,
  cache: true,
  devtool: 'eval-source-map',
  entry: {
    ...base.entry,
    index: [
      'babel-polyfill',
      path.join(__dirname, 'app', 'modules', 'index.js'),
      './app/modules/',
      './app/styles.less',
    ],
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'app.js',
    publicPath: 'https://localhost:3001/public/',
  },
  module: {
    ...base.module,
    loaders: [
      ...base.module.loaders,
      {
        test: /\.(less|css)(\?\S*)?$/,
        exclude: ['node_modules'],
        loader: ExtractTextPlugin.extract(['css-loader', 'less-loader']),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader'],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new WebpackNotifierPlugin({
      title: 'Webpack',
      alwaysNotify: true,
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new ExtractTextPlugin('styles.css'),
    new CleanWebpackPlugin('./public', {}),
  ],
  // externals: {
  //   'TweenLite': 'TweenLite',
  // }
};

export default config;