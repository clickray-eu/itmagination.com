import path from 'path';

export default {
  entry: {
    index: [
      'babel-polyfill',
      'babel-register',
      path.join(__dirname, 'app', 'modules', 'index.js'),
      path.join(__dirname,'app','styles.less')
    ],
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'app.js',
    publicPath: '/hubfs/assets/',
  },
  module: {
    loaders: [],
  },
  plugins: [],
};

