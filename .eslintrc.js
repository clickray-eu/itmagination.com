module.exports = {
    "extends": "airbnb-base",
    "plugins": [
        "import"
    ],
    "globals": {
        "window": true,
        "document": true,
        "$":true
    },
    "rules":{
        "one-var":[0,"always"]
    }   
}