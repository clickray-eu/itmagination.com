import httpProxy from 'http-proxy';
import WebpackDevServer from 'webpack-dev-server';
import https from 'https';
import fs from 'fs';
import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import config from './webpack.dev';


const port = 3001,
  credentials = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
  },
  options = {
    ssl: credentials,
    target: 'https://careers.itmagination.com/',
    autoRewrite: true,
    changeOrigin: true,
    secure: false,
    ignorePath: false,
  };

const apiProxy = httpProxy.createProxyServer();
const app = express();
const compiler = webpack(config);


const compilerConfig = {
  publicPath: config.output.publicPath,
  hot: true,
  https: true,
  open: true,
  stats: {
    colors: true,
    hash: true,
    version: true,
    timings: false,
    assets: true,
    chunks: false,
    modules: false,
    reasons: true,
    children: true,
    source: true,
    errors: true,
    errorDetails: true,
    warnings: true,
    publicPath: true,
  },
};

app.get('*hubfs/assets/app-careers3.js', (req, res) => {
  return apiProxy.web(req, res, { target: 'https://localhost:3003/public/app.js', ignorePath: true });
});
app.get('*/hubfs/assets/styles.css', (req, res) => {
  return apiProxy.web(req, res, { target: 'https://localhost:3003/public/styles.css', ignorePath: true });
});
app.get('*hot-update*', (req, res) => {
  return apiProxy.web(req, res, { target: 'https://localhost:3003', ignorePath: false });
});

app.get('*', (req, res) => {
  return apiProxy.web(req, res, options);
});

new WebpackDevServer(webpack(config), compilerConfig).listen(3003, 'localhost', (err) => {
  if (err) {
    return console.log(err);
  }
  console.log('Webpack server is running, starting compiler...');
});

app.use(webpackDevMiddleware(compiler, {
  ...compilerConfig,

  watchOptions: {
    aggregateTimeout: 1000,
    poll: 1000,
  },
}));

const httpsServer = https.createServer(credentials, app);
httpsServer.listen(port);

console.log(`server listening on https://localhost:${port}`);

