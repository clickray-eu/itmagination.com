import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import base from './webpack.base';


const config = {
  ...base,
  module: {
    ...base.module,
    loaders: [
      ...base.module.loaders,
      {
        test: /\.(less|css)(\?\S*)?$/,
        exclude: ['node_modules'],
        loader: ExtractTextPlugin.extract(['css-loader', 'less-loader']),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader'],
      },
    ],
  },
  plugins: [
    ...base.plugins,
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      compress: {
        warnings: false,
      },
      comments: false,
    }),
    new ExtractTextPlugin('styles.css'),
  ],
};

export default config;
