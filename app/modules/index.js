// import 'store';
// import 'gsap';
// import './shared/index.js';
// import './shared/hotjar.js';
// import './shared/ga.js';
// import './shared/tooltipster';
// import 'fullpage.js';
// import 'jquery';
// import 'jquery-modal';
// import 'awesomplete';
// import 'masonry-layout';
// import 'jquery-circle-progress';
// import '../../node_modules/bxslider/dist/jquery.bxslider.min.js';
// import '../../node_modules/zurb-twentytwenty/js/jquery.event.move.js';
// import 'lodash';
// import Fuse from 'fuse.js';


// //####################################################################
// import homepage from '../pages/home/index.js';
// import inlab from '../pages/inlab/index.js';
// import inlabitm from '../pages/inlabitm/index.js';
// import innovations from '../pages/innovations/index.js';
// import jobs from '../pages/jobs/index.js';
// import joinus from '../pages/joinus/index.js';
// import joinus_lp from '../pages/joinus_lp/index.js';
import careers from '../pages/careers/index.js';
// import services from '../pages/services/index.js';
// import about from '../pages/about/index.js';
// import contact from '../pages/contact/index.js';
// import booster from '../pages/booster/index.js';

const pages = {
    'home' : () => homepage(),
    'inlab' : () => inlab(),
    'inlabitm' : () => inlabitm(),
    'innovations' : () => innovations(),
    'jobs' : () => jobs(),
    'joinus' : () => joinus(),
    'careers' : () => careers(),
    'joinus-landing' : () => joinus_lp(),    
    'services' : () => services(),
    'about' : () => about(),
    'contact' : () => contact(),
    'booster' : () => booster(),
};

function getPage(page = 'home'){
    return pages[page]();
}

$(document).ready(function () {
    getPage(window.itMaginationCurrentPage);
});