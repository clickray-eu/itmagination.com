(function () {
  'use strict';

  var plugins = {};
  var sharedVariables = {};

  var defaultDiacriticsRemovalMap = [
      {'base':'A', 'letters':'\u0104'},
      {'base':'C', 'letters':'\u0106'},
      {'base':'E', 'letters':'\u0118'},
      {'base':'L', 'letters':'\u0141'},
      {'base':'N', 'letters':'\u0143'},
      {'base':'O', 'letters':'\u00D3'},
      {'base':'S', 'letters':'\u015A'},
      {'base':'Z', 'letters':'\u0179\u017B'},
      {'base':'a', 'letters':'\u0105'},
      {'base':'c', 'letters':'\u0107'},
      {'base':'e', 'letters':'\u0119'},
      {'base':'l', 'letters':'\u0142'},
      {'base':'n', 'letters':'\u0144'},
      {'base':'o', 'letters':'\u00F3'},
      {'base':'s','letters':'\u015B'},
      {'base':'z','letters':'\u017A\u017C'}
  ];

  var diacriticsMap = {};
  for (var i=0; i < defaultDiacriticsRemovalMap.length; i++){
    var letters = defaultDiacriticsRemovalMap[i].letters;
    for (var j=0; j < letters.length ; j++){
      diacriticsMap[letters[j]] = defaultDiacriticsRemovalMap[i].base;
    }
  }

  var EVENT_FUNCTIONS = {
    'our-story': function (event) {
      if (event.which === 37) {
        plugins.storySlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.storySlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'awards': function (event) {
      if (!plugins.awardSlider) {
        return;
      }
      if (event.which === 37) {
        plugins.awardSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.awardSlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'updates': function (event) {
      if (!plugins.updateSlider) {
        return;
      }
      if (event.which === 37) {
        plugins.updateSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.updateSlider.nextSliderThrottled();
      } else {
        return;
      }
    }
  };

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
  }

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!plugins.storySlider.reloadSlider) {
        plugins.storySlider.reloadSlider();
      }
      if (!!plugins.valuesSlider.reloadSlider) {
        plugins.valuesSlider.reloadSlider();
      }
      fixStory();
    });
  }

  function dispatchKeyEvents (event) {
    var section = _.last(_.last(window.location.href.split('/')).split('#'));
    var fn = EVENT_FUNCTIONS[section];
    if (!fn) {
      return;
    }
    fn(event);
  }

  function fixStory () {
    var $list = $('.bxslider-story');
    var height = $list.parent().height();
    $list
      .children('li')
      .each(function () {
        $(this).css({'height':height + 'px'});
      });
  }

  function getBoardCardTemplate (entry, key, position) {
    return '<div data-board="' + position + key + '">' +
             '<div class="i13m-about-board-header">' +
               '<img src="//cdn2.hubspot.net/hubfs/2422193/pictures/board/' + entry.image + '.jpg"/>' +
             '</div>' +
             '<div class="i13m-about-board-info">' +
               '<div class="i13m-about-board-name">' + entry.name + '</div>' +
               '<div class="i13m-about-board-post">' + entry.post + '</div>' +
             '</div>' +
           '</div>';
  }

  function getBoardModalTemplate (entry, key, position) {
    return '<div class="i13m-home-board-modal" id="i13m-about-board-' + position + key + '" ' +
              'style="opacity:0;z-index:-99;' +
              'background-image:linear-gradient(90deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0.8) 30%, rgba(255, 255,255, 0) 75%), ' +
              'linear-gradient(90deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.1) 0%), url(//cdn2.hubspot.net/hubfs/2422193/pictures/board/big/' + entry.image + '.jpg);' +
              'background-image:-webkit-linear-gradient(0deg, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 0.8) 30%, rgba(255, 255,255, 0) 75%), ' +
              '-webkit-linear-gradient(0deg, rgba(0, 0, 0, 0.1) 0%, rgba(0, 0, 0, 0.1) 0%), url(//cdn2.hubspot.net/hubfs/2422193/pictures/board/big/' + entry.image + '.jpg);">' +
               '<a class="i13m-modal-close"></a>' +
               '<div class="i13m-home-board-modal-content">' +
                 '<h5>' + entry.name + '</h5>' +
                 '<h6>' + entry.post + '</h6>' +
                 '<div>' + entry.text + '</div>' +
                 '<a href="' + entry.linkedin + '" class="i13m-linkedin" target="_new"></a>' +
               '</div>' +
             '</div>';
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();

    sharedVariables.screenWidth = $(window).width();

    if (sharedVariables.screenWidth <= 767) {
      sharedVariables.awardSlides = 1;
    } else if (sharedVariables.screenWidth <= 1024) {
      sharedVariables.awardSlides = 2;
    } else {
      sharedVariables.latestSlides = 4;
    }
  }

  function getValuesSlideTemplate (entry) {
    return '<li style="background-image:linear-gradient(rgba(255, 255, 255, 0.75),rgba(255, 255, 255, 0.75)), url(' + entry.image + ');">' +
        '<h6>' + entry.name + '</h6>' +
        '<div>' + entry.text + '</div>' +
      '</li>';
  }

  function initPlugins() {
    plugins.storySlider = $('.bxslider-story').bxSlider({
      maxSlides: 1,
      minSlides: 1,
      moveSlides: 1,
      controls: false
    });
    setSliderControls(plugins.storySlider, '.i13m-left-button.story', '.i13m-right-button.story');

    if (sharedVariables.awardSlides) {
      plugins.awardSlider = $('.i13m-about-awards-awards').bxSlider({
        maxSlides: sharedVariables.awardSlides,
        minSlides: sharedVariables.awardSlides,
        moveSlides: sharedVariables.awardSlides,
        slideMargin: 10,
        slideWidth: 274,
        controls: false
      });
      setSliderControls(plugins.awardSlider, '.i13m-left-button.awards', '.i13m-right-button.awards');
    }

    $('.bx-prev, .bx-next').removeAttr('href');
    $('.i13m-updates-grid').i13mPagination({
      controlsSelector: '.i13m-pagination-controls'
    });
  }

  function onReady () {
    getScreenVariables();
    initPlugins();
    renderSections();
    addSliderResizer();
    addKeyEventWatchers();
  }

  function parseBoardData () {
    sharedVariables.boardData = sharedVariables.boardData || { top: {}, bottom: {}};
    $('.i13m-about-board-top-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.boardData.top[index] = {
          name: $(this).children().first().html(),
          post: $(this).children().filter(function (index) { return index === 1; }).html(),
          text: $(this).children().filter(function (index) { return index === 2; }).html(),
          image: removeDiacritics($(this).children().first().html()).toLowerCase().replace(/[\s]/gi, '-'),
          linkedin: $(this).children().last().html()
        };
      });
    $('.i13m-about-board-bottom-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.boardData.bottom[index] = {
          name: $(this).children().first().html(),
          post: $(this).children().filter(function (index) { return index === 1; }).html(),
          text: $(this).children().filter(function (index) { return index === 2; }).html(),
          image: removeDiacritics($(this).children().first().html()).toLowerCase().replace(/[\s]/gi, '-'),
          linkedin: $(this).children().last().html()
        };
      });
  }

  function parseValuesData () {
    sharedVariables.valuesData = sharedVariables.valuesData || {};
    $('.i13m-about-values-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.valuesData[index] = {
          name: $(this).children().first().html(),
          text: $(this).children().filter(function (index) { return index === 1; }).html(),
          image: $(this).children().last().html()
        };
      });
  }

  function removeDiacritics (str) {
    return str.replace(/[^\u0000-\u007E]/g, function(a){
      return diacriticsMap[a] || a;
    });
  }

  function renderBoard () {
    parseBoardData();
    renderBoardElements();
    setupBoardWatchers();
  }

  function renderBoardElements () {
    var $topContainer = $('.i13m-about-board-top');
    var $bottomContainer = $('.i13m-about-board-bottom');
    var $modalContainer = $('.i13m-about-board').find('.i13m-container');
    _.each(sharedVariables.boardData.top, function (entry, key) {
      var cardTemplate = getBoardCardTemplate(entry, key, 'top');
      var modalTemplate = getBoardModalTemplate(entry, key, 'top');
      $topContainer.append(cardTemplate);
      $modalContainer.append(modalTemplate);
    });
    _.each(sharedVariables.boardData.bottom, function (entry, key) {
      var cardTemplate = getBoardCardTemplate(entry, key, 'bottom');
      var modalTemplate = getBoardModalTemplate(entry, key, 'bottom');
      $bottomContainer.append(cardTemplate);
      $modalContainer.append(modalTemplate);
    });
  }

  function renderDownloads () {
    setupPolicyWatchers();
  }

  function renderSections () {
    renderBoard();
    renderValues();
    renderDownloads();
  }

  function renderValues () {
    parseValuesData();
    renderValuesElements();
    setupValuesPlugins();
  }

  function renderValuesElements () {
    var $container = $('.bxslider-values');
    _.each(sharedVariables.valuesData, function (entry) {
      var slideTemplate = getValuesSlideTemplate(entry);
      $container.append(slideTemplate);
    });
  }

  function setSliderControls (slider, leftSelector, rightSelector) {
    slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
    slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
    $(leftSelector).on('click', function () {
      slider.prevSliderThrottled();
      return false;
    });
    $(rightSelector).on('click', function () {
      slider.nextSliderThrottled();
      return false;
    });
  }

  function setupBoardWatchers () {
    $('.i13m-about-board-header')
      .parent('div')
      .each(function () {
        var $this = $(this);
        $this.click(function () {
          var $modal = $('#i13m-about-board-' + $(this).data('board'));
          $modal.css({ 'z-index': 199 });
          TweenMax.to($modal, 0.3, { autoAlpha: 1 });
        });
      });
    $('.i13m-modal-close')
      .each(function () {
        var $this = $(this);
        var $modal = $this.parent();
        $this.click(function () {
          TweenMax.to($modal, 0.3, {
            autoAlpha: 0,
            onComplete: function () {
              $modal.css({ 'z-index': -99 });
            }
           });
        });
      });
  }

  function setupPolicyWatchers () {
    $('.i13m-policy-button').click(function () {
      var $modal = $('.i13m-about-policy-modal');
      $modal.css({ 'z-index': 199 });
      TweenMax.to($modal, 0.3, { autoAlpha: 1 });
    });
    $('.i13m-about-policy-modal-close').click(function () {
      var $modal = $(this).parent();
      var timeLine = new TimelineMax();
      timeLine
        .to($modal, 0.3, { autoAlpha: 0 })
        .to($modal, 0.001, { zIndex: -99 });
    });
  }

  function setupValuesPlugins () {
    plugins.valuesSlider = $('.bxslider-values').bxSlider({
      mode: 'vertical',
      controls: false
    });
  }

  $(document).ready(onReady);
}());
