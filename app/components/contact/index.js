(function () {
  'use strict';

  var tooltips = [];
  var IMAGE_WIDTH = 1128;
  var IMAGE_HEIGHT = 557;
  var IMAGE_ASPECT_RATIO = IMAGE_WIDTH / IMAGE_HEIGHT;
  var TOOLTIP_SIZE = 0.01689;
  var padding = 0.1;
  var sharedVariables = {};

  function parseTooltips () {
    $('#i13m-map-tooltips')
      .children()
      .each(function () {
        if (!this.dataset.xCoordinate || !this.dataset.yCoordinate) {
          return;
        }
        tooltips.push({
          x: this.dataset.xCoordinate,
          y: this.dataset.yCoordinate,
          el: $(this)
        });
      });
  }

  function renderContact () {
    setupContactWatchers();
  }

  function renderSections () {
    renderContact();
  }

  function setupContactWatchers () {

    var setSuccess = setInterval(function () {
      if (!$('.i13m-contact-form').find('.hs-form').length) {
        return;
      } else {
        clearInterval(setSuccess);
        $('.i13m-contact-info-container')
          .find('form')
            .on('submit', function(e) {
              sharedVariables.areAllFieldsOK = true;
              var $form = $(this);
              $form
                .find('input[required]')
                .each(function () {
                  var $input = $(this);
                  if ($input.attr('class').indexOf('error') > -1) {
                    sharedVariables.areAllFieldsOK = false;
                  }
                });
              if (sharedVariables.areAllFieldsOK) {
                ga('send', 'event', 'contact', 'formularzKontaktowy');
              } else {
                e.preventDefault();
                return false;
              }
          });
      }
    }, 100);
    $('.i13m-contact-modal-open')
      .click(function () {
        var $modal = $('.i13m-contact-contact-modal');
        $modal.css({ 'z-index': 50 });
        TweenMax.to($modal, 0.3, { autoAlpha: 1 });
      });
    $('.i13m-job-success-dismiss')
      .click(function () {
        var $modal = $('.i13m-contact-contact-modal');
        TweenMax.to($modal, 0.3, {
          autoAlpha: 0,
          onComplete: function () {
            $modal.css({ 'z-index': -99 });
          }});
      });
  }

  function updateMap () {
    var html = document.documentElement;

    var screenHeight = Math.max(
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight
    );

    var screenWidth = Math.max(
      html.clientWidth,
      html.scrollWidth,
      html.offsetWidth
    );
    var imageContainerWidth = screenWidth - 2 * padding * screenHeight;
    var imageContainerHeight = screenHeight * (1 - 2 * padding) - 72;
    var containerAspectRatio = imageContainerWidth / imageContainerHeight;
    var actualImageWidth;
    var actualImageHeight;
    var leftMargin;
    var topMargin;
    var imageScaleRatio;
    if (containerAspectRatio > IMAGE_ASPECT_RATIO) {
      actualImageHeight = imageContainerHeight;
      actualImageWidth = IMAGE_ASPECT_RATIO * actualImageHeight;
    } else {
      actualImageWidth = imageContainerWidth;
      actualImageHeight = actualImageWidth / IMAGE_ASPECT_RATIO;
    }
    imageScaleRatio = actualImageWidth / IMAGE_WIDTH;
    leftMargin = (screenWidth - actualImageWidth) / 2;
    topMargin = (screenHeight - actualImageHeight) / 2;

    _.each(tooltips, function (tooltip) {
      updateCoordinates(tooltip, screenWidth, screenHeight, leftMargin, topMargin, imageScaleRatio);
    });

  }

  function updateCoordinates (tooltip, screenWidth, screenHeight, leftMargin, topMargin, imageScaleRatio) {
    var left = ((tooltip.x * imageScaleRatio - TOOLTIP_SIZE * screenHeight) + leftMargin) / screenWidth;
    var top = ((tooltip.y * imageScaleRatio - TOOLTIP_SIZE * screenHeight) + topMargin) / screenHeight;
    tooltip.el.css({
      left: left * 100 + '%',
      top: top * 100 + '%'
    });
  }


  $(document).ready(function () {
    $('.i13m-map-tooltip.boston').tooltipster({
      animationDuration: 150,
      interactive: true
    });
    $('.i13m-map-tooltip.poland').tooltipster({
      animationDuration: 150,
      interactive: true,
      side: 'right',
      functionBefore: function () {
        TweenMax.to($('.poland.i13m-map-tooltip').children('p'), 0.3, { autoAlpha: 0 });
      },
      functionAfter: function () {
        TweenMax.to($('.poland.i13m-map-tooltip').children('p'), 0.3, { autoAlpha: 1 });
      },
      trigger: 'custom',
      triggerOpen: {
        mouseenter: true
      },
      triggerClose: {
        originClick: true
      }
    });
    $('.i13m-map-tooltip-small').tooltipster({
      animationDuration: 150,
      interactive: true,
      side: 'left'
    });
    parseTooltips();
    updateMap();

    $(window).on('window:resize', function () {
      updateMap();
    });
    renderSections();
  });
}());
