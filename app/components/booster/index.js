(function () {
  'use strict';

  var COMMENT_DICTIONARY = {
    en: {
      single: 'comment',
      smallPlural: 'comments',
      plural: 'comments'
    },
    pl: {
      single: 'komentarz',
      smallPlural: 'komentarze',
      plural: 'komentarzy'
    }
  };

  var sliders = {};
  var $grid;

  var scrollIndex = 3;
  // var previousScrollPosition;
  var sharedVariables = {};
  var postGallery;

  var checkIfDisqusLoaded = setInterval(processComments, 1000);

  function addInfiniteScroll () {
    var $window = $(window);
    $window.scroll(function () {
      if ($window.scrollTop !== 0 && $window.scrollTop() + $window.height() > $(document).height() - 100) {
        applyScrollingByIndex(++scrollIndex);
      }
    });
  }

  function addLinkWatchers () {
    var width = window.innerWidth / 3;
    var height = window.innerHeight / 3;
    $('.share')
      .children('a')
      .each(function () {
        $(this).click(function () {
          window.open(this.href, 'newwindow', 'width=' + width + ', height=' + height + ', top=' + height + ', left=' + width);
          return false;
        });
      });
  }

  function addPagerControls () {
    $('.i13m-booster-latest-container')
      .children('.i13m-booster-pager')
      .children('a')
      .each(function () {
        var $button = $(this);
        $button.removeClass('active');
        $button.click(function (e) {
          sliders.main.goToSlide($button.data('slide-index'));
          $button.addClass('active');
          e.preventDefault();
        });
      });
  }

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!sliders.main.reloadSlider) {
        sliders.main.reloadSlider();
      }
    });
  }

  function applyPagerStyles () {
    _.each(sliders, function (slider) {
      _.bind(sortOutPagerStyling, slider, undefined, undefined, 0)();
    });
  }

  function applyScrollingByIndex (scrollIndex) {
    $('.i13m-booster-body')
      .children('.grid-item')
      .each(function (index) {
        var $el = $(this);
        if (index > scrollIndex) {
          $el.css({
            display: 'none',
            opacity: 0
          });
          $grid.masonry('layout');
        } else {
          if ($el.css('display') === 'none') {
            $el.css({ display: 'block' });
            $grid.masonry('layout');
            TweenMax.to($el, 0.3, { autoAlpha: 1 });
          }
        }
      });
  }

  // function fixSuccessMsg () {
  //   $('.i13m-success-msg')
  //     .css({'position': 'fixed'});
  // }

  function hideGallery () {
    TweenMax.to($('.i13m-booster-post-gallery'), 0.3, { autoAlpha: 0, zIndex: -999});
  }

  function initPlugins () {
    $grid = $('.grid');
    sliders.main = $('.bxslider').bxSlider({
      controls: false,
      pager: false,
      maxSlides: 3,
      minSlides: 3,
      moveSlides: 1,
      responsive: true,
      auto: true,
      autoStart: true,
      onSlideBefore: sortOutPagerStyling
    });

    $('.bxslider-post')
      .each(function (index) {
        sliders[index] = $(this).bxSlider({
          controls: false,
          pager: false,
          maxSlides: 4,
          minSlides: 1,
          moveSlides: 1,
          responsive: true,
          auto: true,
          autoStart: true,
          onSlideBefore: sortOutPagerStyling
        });
      });
    if (!!sharedVariables.postImages.length) {
      postGallery = $('.bxslider-image')
      .bxSlider({
        maxSlides: 1,
        pager: false,
        minSlides: 1,
        moveSlides: 1,
        responsive: true,
        nextText: '>',
        prevText: '<'
      });
    }

    $('.i13m-social-tooltip')
      .tooltipster({
        animationDuration: 150,
        interactive: true,
        side: 'bottom',
        trigger: 'custom',
        maxWidth: 100,
        triggerOpen: {
            click: true,
            mouseenter: true,
            tap: true
        },
        triggerClose: {
            click: true,
            scroll: true,
            mouseleave: true,
            tap: true
        }
      });

    $grid.masonry({
      gutter: 30,
      itemSelector: '.grid-item',
      transitionDuration: 0
    });

  }

  function parseImagesInText() {
    sharedVariables.postImages = sharedVariables.postImages || [];
    $('.i13m-booster-post-contents')
      .find('img')
      .each(function (index) {
        sharedVariables.postImages.push(_.first($(this).attr('src').split('?')));
        $(this).data('gallery-index', index);
        $(this).click(showGallery);
      });
  }

  function processComments () {
    var language = window.location.href.split('/')[3];
    var template = '<i class="fa fa-comments-o"></i>';
    var dictionary = COMMENT_DICTIONARY[language];
    $('.i13m-disqus-count')
      .each(function () {
        var comments = parseInt($(this).html() || 0);
        var noun = (comments < 10 || comments > 20) && comments % 10 > 1 && comments % 10 < 5 ?
          dictionary.plural :
            comments === 1 ?
              dictionary.single :
              dictionary.smallPlural;
        $(this)
          .next('a')
          .html(template + ' ' + comments + ' ' + noun);
      });
  }

  function processImages () {
    _.each(sharedVariables.postImages, function (image) {
      var template = '<li class="slide"><img src="' + image + '"/></li>';
      $('.bxslider-image').append(template);
    });
  }

  function setWatchers () {
    addLinkWatchers();
    $('.i13m-booster-post-gallery-close').click(hideGallery);

      var setSuccess = setInterval(function () {
        if (!$('.i13m-booster-contact-form').find('.hs-form').length) {
          return;
        } else {
          clearInterval(setSuccess);
          $('.i13m-booster-contact-form')
            .find('form')
            .on('submit', function(e) {
              var areAllFieldsOK = true;
              $(this)
                .find('input[required]')
                .each(function () {
                  if ($(this).attr('class').indexOf('error') > -1) {
                     areAllFieldsOK = false;
                  }
                });
                if (areAllFieldsOK) {
                } else {
                  e.preventDefault();
                  return false;
                }
            });
        }
      }, 100);
  }

  function showGallery () {
    var $gallery = $('.i13m-booster-post-gallery');
    var index = $(this).data('gallery-index');
    postGallery.goToSlide(index);
    $gallery.css({ 'z-index': 50} );
    TweenMax.to($gallery, 0.3, { autoAlpha: 1});
  }

  function sortOutPagerStyling ($slideElement, oldIndex, newIndex) {
    $(this)
      .parent()
      .parent()
      .next('.i13m-booster-pager')
      .children('a')
      .removeClass('active')
      .filter(function () {
        return $(this).data('slide-index') === newIndex;
      })
      .addClass('active');
  }

  $(document).ready(function () {
    parseImagesInText();
    processImages();
    initPlugins();
    addSliderResizer();
    addPagerControls();
    applyPagerStyles();
    applyScrollingByIndex(scrollIndex);
    addInfiniteScroll();
    setWatchers();
  });
}());
