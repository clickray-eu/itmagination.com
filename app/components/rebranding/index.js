(function () {
  'use strict';

  var plugins = {};
  var sharedVariables = {};

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!plugins.promiseSlider.reloadSlider) {
        plugins.promiseSlider.reloadSlider();
      }
      if (!!plugins.symbologySlider.reloadSlider) {
        plugins.symbologySlider.reloadSlider();
      }
      if (!!plugins.cultureSlider.reloadSlider) {
        plugins.cultureSlider.reloadSlider();
      }
    });
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();

    sharedVariables.screenWidth = $(window).width();

    if (sharedVariables.screenWidth <= 767) {
      sharedVariables.brandSlides = 1;
      sharedVariables.symbologySlides = 1;
    } else if (sharedVariables.screenWidth <= 1024) {
      sharedVariables.brandSlides = 1;
      sharedVariables.symbologySlides = 1;
    } else {
    }
  }

  function initPlugins () {
    plugins.promiseSlider = $('.bxslider-promise').bxSlider({
      mode: 'vertical',
      controls: false
    });
    plugins.symbologySlider = $('.bxslider-symbology').bxSlider({
      mode: 'vertical',
      controls: false
    });
    plugins.cultureSlider = $('.bxslider-culture').bxSlider({
      mode: 'vertical',
      controls: false
    });
    if (sharedVariables.brandSlides) {
      $('.i13m-vertical-separator').remove();

      if (sharedVariables.brandSlides) {
        plugins.brandSlider = $('.i13m-brand-slider').bxSlider({
          controls: false,
          maxSlides: sharedVariables.brandSlides,
          minSlides: sharedVariables.brandSlides,
          moveSlides: sharedVariables.brandSlides,
          slideWidth: 240,
          slideMargin: 10
        });
        setSliderControls(plugins.brandSlider, '.i13m-left-button.brands', '.i13m-right-button.brands');
      }

      if (sharedVariables.symbologySlides) {
        plugins.symbologySlider = $('.i13m-symbology-slider').bxSlider({
          controls: false,
          maxSlides: sharedVariables.symbologySlides,
          minSlides: sharedVariables.symbologySlides,
          moveSlides: sharedVariables.symbologySlides,
          slideWidth: 240,
          slideMargin: 10
        });
        setSliderControls(plugins.symbologySlider, '.i13m-left-button.symbology', '.i13m-right-button.symbology');
      }

    }
  }

  function onReady () {
    getScreenVariables();
    initPlugins();
    addSliderResizer();
    renderSections();
  }

  function renderContact () {
    setupContactWatchers();
  }

  function renderSections () {
    renderContact();
  }

  function setSliderControls (slider, leftSelector, rightSelector) {
    slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
    slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
    $(leftSelector).on('click', function () {
      slider.prevSliderThrottled();
      return false;
    });
    $(rightSelector).on('click', function () {
      slider.nextSliderThrottled();
      return false;
    });
  }


  function setupContactWatchers () {
    var setSuccess = setInterval(function () {
      if (!$('.i13m-home-contact-form').find('.hs-form').length) {
        return;
      } else {
        clearInterval(setSuccess);
        $('.i13m-home-contact-form')
          .find('form')
          .on('submit', function(e) {
            var areAllFieldsOK = true;
            $(this)
              .find('input[required]')
              .each(function () {
                if ($(this).attr('class').indexOf('error') > -1) {
                   areAllFieldsOK = false;
                }
              });
              if (areAllFieldsOK) {
              } else {
                e.preventDefault();
                return false;
              }
          });
      }
    }, 100);
  }

  $(document).ready(onReady);
  $(window).load(function() {
    $('.i13m-logo-diff').twentytwenty();
  });
}());
