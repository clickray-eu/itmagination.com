(function () {
  'use strict';

  var plugins = {};
  var sharedVariables = {};

  var EVENT_FUNCTIONS = {
  };

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
  }

  function dispatchKeyEvents (event) {
    var section = _.last(_.last(window.location.href.split('/')).split('#'));
    var fn = EVENT_FUNCTIONS[section];
    if (!fn) {
      return;
    }
    fn(event);
  }

  function getScreenVariables () {
  }

  function initPlugins() {
  }

  function onReady () {
    getScreenVariables();
    initPlugins();
    addKeyEventWatchers();
  }

  $(document).ready(onReady);
}());
