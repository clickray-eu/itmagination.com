(function () {
  'use strict';

  var plugins = {};
  var sharedVariables = {};

  var EVENT_FUNCTIONS = {
    'services': function (event) {
      var direction;
      var newIndex;
      if (event.which === 37) {
        direction = -1;
      } else if (event.which === 39) {
        direction = 1;
      } else {
        return;
      }
      newIndex = Math.max(0, Math.min(_.keys(sharedVariables.serviceData).length - 1, sharedVariables.serviceIndex + direction));
      chooseService(newIndex);
    },
    'meet-our-team': function (event) {
      if (!plugins.teamSlider) {
        return;
      }
      if (event.which === 37) {
        plugins.teamSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.teamSlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'satisfied-customers': function (event) {
      if (!plugins.customerSlider) {
        return;
      }
      if (event.which === 37) {
        plugins.customerSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.customerSlider.nextSliderThrottled();
      } else {
        return;
      }
    }
  };

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
  }

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!plugins.customerSlider.reloadSlider) {
        plugins.customerSlider.reloadSlider();
      }
    });
  }

  function chooseService (index) {
    sharedVariables.serviceIndex = index;
    selectServiceContent(index);
    selectServiceButton(index);
  }

  function dispatchKeyEvents (event) {
    var section = _.last(_.last(window.location.href.split('/')).split('#'));
    var fn = EVENT_FUNCTIONS[section];
    if (!fn) {
      return;
    }
    fn(event);
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();
    sharedVariables.screenWidth = $(window).width();

    if (sharedVariables.screenWidth <= 767) {
      sharedVariables.customerSlides = 2;
      sharedVariables.teamSlides = 1;
    } else if (sharedVariables.screenWidth <= 1024) {
      sharedVariables.customerSlides = 4;
      sharedVariables.teamSlides = 2;
    } else {
      sharedVariables.customerSlides = 6;
    }
  }

  function getSharedVariables () {
    sharedVariables.page = window.location.href.split('/')[4] ? _.first(window.location.href.split('/')[4].split('#')) : _.first(window.location.href.split('/')[2].split('.'));
    sharedVariables.subPage = window.location.href.split('/')[5] && _.first(window.location.href.split('/')[5].split('#'));
  }

  function initPlugins () {
    plugins.customerSlider = $('.bxslider-customers').bxSlider({
      maxSlides: sharedVariables.customerSlides,
      minSlides: sharedVariables.customerSlides,
      moveSlides: sharedVariables.customerSlides,
      controls: false,
      slideWidth: 150
    });
    setSliderControls(plugins.customerSlider, '.i13m-left-button.customers', '.i13m-right-button.customers');
    if (sharedVariables.teamSlides) {
      plugins.teamSlider = $('.i13m-services-team-widget').bxSlider({
        maxSlides: sharedVariables.teamSlides,
        minSlides: sharedVariables.teamSlides,
        moveSlides: sharedVariables.teamSlides,
        controls: false,
        slideWidth: 200,
        slideMargin: 10
      });
      setSliderControls(plugins.teamSlider, '.i13m-left-button.teams', '.i13m-right-button.teams');
    }
    $('.bx-prev, .bx-next').removeAttr('href');
  }

  function onReady () {
    getScreenVariables();
    getSharedVariables();
    processSharedVariables();
    renderSections();
    addSliderResizer();
    setupSharedWatchers();
    initPlugins();
  }

  function parseMainData() {
    sharedVariables.mainData = sharedVariables.mainData || {};
    $('.i13m-services-main-data')
      .find('tr')
      .each(function () {
        var competence = $(this).children().first().html();
        sharedVariables.mainData[competence.toLowerCase().replace(/[\s]/gi, '-')] = {
          competence: competence,
          text: $(this).children().filter(function (index) { return index === 1; }).html(),
          leader: $(this).children().filter(function (index) { return index === 2; }).html(),
          post: $(this).children().last().html()
        };
      });
  }

  function parseServiceData () {
    sharedVariables.serviceData = sharedVariables.serviceData || {};
    $('.i13m-services-service-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.serviceData[index] = {
          name: $(this).children().first().html(),
          text: $(this).children().last().html()
        };
      });
  }

  function parseTeamData () {
    sharedVariables.teamData = sharedVariables.teamData || {};
    $('.i13m-services-team-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.teamData[index] = {
          name: $(this).children().first().html(),
          post: $(this).children().filter(function (index) { return index === 1; }).html(),
          imageUrl: $(this).children().filter(function (index) { return index === 2; }).html(),
          text: $(this).children().last().html()
        };
      });
  }

  function processSharedVariables () {
    sharedVariables.competence = _.first(window.location.href.split('/')[5].split('#'));
  }

  function renderMain() {
    parseMainData();
    renderMainElements();
    setupMainWatchers();
  }

  function renderMainElements() {
    var content = sharedVariables.mainData[sharedVariables.competence];
    var classPrefix = '.i13m-services-main-';
    _.each(content, function (entry, key) {
      $(classPrefix + key).html(entry);
    });
  }

  function renderSections () {
    renderContact();
    renderMain();
    renderServices();
    renderTeam();
  }

  function renderContact () {
    setupContactWatchers();
  }

  function renderServiceElements () {
    var $buttons = $('.i13m-services-service-buttons');
    var $texts = $('.i13m-services-service-texts');
    _.each(sharedVariables.serviceData, function (entry) {
      var buttonTemplate = '<button>' + entry.name + '</button>';
      var textTemplate =
      '<div>' +
        '<div class="i13m-services-service-heading">' + entry.name + '</div>' +
        '<div class="i13m-services-service-text">' + entry.text + '</div>' +
      '</div>';
      $buttons.append(buttonTemplate);
      $texts.append(textTemplate);
    });
  }

  function renderServices () {
    parseServiceData();
    renderServiceElements();
    setupServiceWatchers();
    chooseService(0);
  }

  function renderTeam () {
    parseTeamData();
    renderTeamElements();
    setupTeamWatchers();
  }

  function renderTeamElements () {
    var $container = $('.i13m-services-team-widget');
    $container
      .children()
      .filter(function (index) {
        return index;
      })
      .remove();
    _.each(sharedVariables.teamData, function (entry, key) {
      if (!parseInt(key)) {
        return;
      }
      var template =
      '<div>' +
        '<div class="i13m-services-team-header">' +
          '<img src="' + entry.imageUrl + '" />' +
          '<p>' + entry.text + '</p>' +
        '</div>' +
        '<div class="i13m-services-team-info">' +
          '<div class="i13m-service-team-name">' + entry.name + '</div>' +
          '<div class="i13m-service-team-post">' + entry.post + '</div>' +
        '</div>' +
      '</div>';
      $container.append(template);
    });

  }

  function selectServiceButton (chosenIndex) {
    var selectedClass = 'selected';
    $('.i13m-services-service-buttons')
      .children('button')
      .removeClass(selectedClass)
      .filter(function (index) {
        return index === chosenIndex;
      })
      .addClass(selectedClass);
  }

  function selectServiceContent (selectedIndex) {
    var timeLine = new TimelineMax();
    var $container = $('.i13m-services-service-texts');
    timeLine
      .to($container.children(), 0.3, { autoAlpha: 0 });
    $container
      .each(function () {
          var $chosen = $(this)
                          .children()
                          .filter(function (index) {
                            return index === selectedIndex;
                          });
          timeLine
            .to($chosen, 0.3, { autoAlpha: 1 }, '-=0.3');
      });

  }

  function setSliderControls (slider, leftSelector, rightSelector) {
    slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
    slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
    $(leftSelector).on('click', function () {
      slider.prevSliderThrottled();
      return false;
    });
    $(rightSelector).on('click', function () {
      slider.nextSliderThrottled();
      return false;
    });
  }

  function setupContactWatchers () {

    var setSuccess = setInterval(function () {
      if (!$('.i13m-services-contact-form').find('.hs-form').length) {
        return;
      } else {
        clearInterval(setSuccess);
        $('.i13m-services-contact-form')
        .find('form')
        .on('submit', function(e) {
          var areAllFieldsOK = true;
          $(this)
            .find('input[required]')
            .each(function () {
              if ($(this).attr('class').indexOf('error') > -1) {
                areAllFieldsOK = false;
              }
            });
            if (areAllFieldsOK) {
              ga('send', 'event', sharedVariables.subPage, 'formularzKontaktowy');
            } else {
              e.preventDefault();
              return false;
            }
        });
      }
    }, 100);
  }

  function setupMainWatchers () {
    $('.i13m-services-main-read')
      .click(function () {
        var $modal = $('.i13m-services-main-modal');
        $modal.css({ 'z-index': 199 });
        TweenMax.to($modal, 0.3, { autoAlpha: 1 });
      });
    $('.i13m-modal-close.service-main')
      .click(function () {
        var $this = $(this);
        var $modal = $this.parent();
        var timeLine = new TimelineMax();
        timeLine
          .to($modal, 0.3, { autoAlpha: 0 })
          .to($modal, 0.001, { zIndex: -99 });
      });
  }

  function setupServiceWatchers () {
    var $modal = $('.i13m-services-service-modal');
    if (sharedVariables.screenWidth > 1024) {
      $('.i13m-services-service-buttons')
      .children('button').click(function () {
          chooseService($(this).index());
        });
    } else {
      $('.i13m-services-service-buttons.i13m-button-group-vertical')
        .children('button').click(function () {
            $modal.css({ 'z-index': 199 });
            TweenMax.to($modal, 0.3, { autoAlpha: 1 });
          });
    }
  }

  function setupSharedWatchers () {
    addKeyEventWatchers();
  }

  function setupTeamWatchers () {
    $('.i13m-services-team-header')
      .each(function () {
        $(this)
          .hover(function () {
            TweenMax.to($(this).children('img'), 0.3, { autoAlpha: 0, zIndex: -99 });
            TweenMax.to($(this).children('p'), 0.3, { autoAlpha: 1, zIndex: 99 }, '-=0.3');
          }, function () {
            TweenMax.to($(this).children('p'), 0.3, { autoAlpha: 0, zIndex: -99 });
            TweenMax.to($(this).children('img'), 0.3, { autoAlpha: 1, zIndex: 99 }, '-=0.3');
          });
        });
  }

  $(document).ready(onReady);
}());
