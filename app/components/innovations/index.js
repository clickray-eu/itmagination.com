(function () {
  'use strict';

  var plugins = {};
  var sharedVariables = {};

  var EVENT_FUNCTIONS = {
    tagetik: {
      'choose-tagetik': function (event) {
        if (event.which === 37) {
          plugins.chooseSlider.prevSliderThrottled();
        } else if (event.which === 39) {
          plugins.chooseSlider.nextSliderThrottled();
        } else {
          return;
        }
      }
    },
    clio: {
      'processes': function (event) {
        var direction;
        var newIndex;
        if (event.which === 37) {
          direction = -1;
        } else if (event.which === 39) {
          direction = 1;
        } else {
          return;
        }
        newIndex = Math.max(0, Math.min(_.keys(sharedVariables.clioProcessesData).length - 1, sharedVariables.clioProcessIndex + direction));
        chooseClioProcess(newIndex);
      },
      'performance': function (event) {
        if (event.which === 37) {
          plugins.performanceSlider.prevSliderThrottled();
        } else if (event.which === 39) {
          plugins.performanceSlider.nextSliderThrottled();
        } else {
          return;
        }
      }
    }
  };

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
  }

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!plugins.chooseSlider && !!plugins.chooseSlider.reloadSlider) {
        plugins.chooseSlider.reloadSlider();
      }
      if (!!plugins.performanceSlider && !!plugins.performanceSlider.reloadSlider) {
        plugins.performanceSlider.reloadSlider();
      }
    });
  }

  function chooseClioProcess (index) {
    var processData = sharedVariables.clioProcessesData[index];
    var timeLine = new TimelineMax();
    var $processes = $('.i13m-processes');
    sharedVariables.clioProcessIndex = index;
    timeLine
      .to($processes, 0.3, { autoAlpha: 0, onComplete: function () {
        $processes
          .empty()
          .append('<h5>' + processData.name + '</h5>')
          .append('<div>' + processData.text + '</div>');
        chooseClioProcessButton(index);
      }}, 'one')
      .to($processes, 0.3, { autoAlpha: 1 }, 'two');
  }

  function chooseClioProcessButton (selectedIndex) {
    var $buttons = $('.i13m-clio-processes').find('.i13m-clio-processes-buttons');
    $buttons
      .children()
      .removeClass('selected')
      .filter(function (index) {
        return index === selectedIndex;
      })
      .addClass('selected');
  }

  function dispatchKeyEvents (event) {
    var section = _.last(_.last(window.location.href.split('/')).split('#'));
    var subpage = _.first(_.last(window.location.href.split('/')).split('#'));
    var fn = EVENT_FUNCTIONS[subpage] && EVENT_FUNCTIONS[subpage][section];
    if (!fn) {
      return;
    }
    fn(event);
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();
    sharedVariables.screenWidth = $(window).width();

    if (sharedVariables.screenWidth <= 767) {
      sharedVariables.processSlides = 1;
      sharedVariables.managementSlides = 1;
      sharedVariables.benefitSlides = 1;
      sharedVariables.trustSlides = 1;
    } else if (sharedVariables.screenWidth <= 1024) {
      sharedVariables.managementSlides = 1;
      sharedVariables.benefitSlides = 1;
      sharedVariables.trustSlides = 1;
    } else {
    }
  }

  function getSharedVariables () {
    sharedVariables.page = window.location.href.split('/')[4] ? _.first(window.location.href.split('/')[4].split('#')) : _.first(window.location.href.split('/')[2].split('.'));
    sharedVariables.subPage = _.first(window.location.href.split('/')[5].split('#'));
  }

  function initPlugins () {
    if (sharedVariables.subPage === 'tagetik') {
      if ($('.bxslider-tagetik').length) {
        plugins.chooseSlider = $('.bxslider-tagetik').bxSlider({
          maxSlides: 1,
          minSlides: 1,
          moveSlides: 1,
          controls: false,
        });
        setSliderControls(plugins.chooseSlider, '.i13m-left-button.tagetik', '.i13m-right-button.tagetik');
      }
      if (sharedVariables.managementSlides) {
        plugins.managementSlider = $('.i13m-tagetik-management-animation-mobile-container').bxSlider({
          maxSlides: sharedVariables.managementSlides,
          minSlides: sharedVariables.managementSlides,
          moveSlides: sharedVariables.managementSlides,
          controls: false,
          slideWidth: 125,
          slideMargin: 10
        });
        setSliderControls(plugins.managementSlider, '.i13m-left-button.management', '.i13m-right-button.management');
      }
      if (sharedVariables.processSlides) {
        plugins.processSlider = $('.i13m-tagetik-gauges').bxSlider({
          maxSlides: 1,
          minSlides: 1,
          moveSlides: 1,
          controls: false,
          slideWidth: 290
        });
        setSliderControls(plugins.processSlider, '.i13m-left-button.process', '.i13m-right-button.process');
      }
      if (sharedVariables.benefitSlides) {
        plugins.benefitSlider = $('.i13m-tagetik-benefits-table-mobile').bxSlider({
          maxSlides: sharedVariables.benefitSlides,
          minSlides: sharedVariables.benefitSlides,
          moveSlides: sharedVariables.benefitSlides,
          controls: false,
          slideWidth: 192
        });
        setSliderControls(plugins.benefitSlider, '.i13m-left-button.benefit', '.i13m-right-button.benefit');
      }
    }
    if (sharedVariables.subPage === 'clio') {
      plugins.performanceSlider = $('.bxslider-performance').bxSlider({
        maxSlides: 1,
        minSlides: 1,
        moveSlides: 1,
        controls: false
      });
      setSliderControls(plugins.performanceSlider, '.i13m-left-button.clio', '.i13m-right-button.clio');
      if (sharedVariables.trustSlides) {
        plugins.trustSlider = $('.i13m-clio-trusted').bxSlider({
          maxSlides: sharedVariables.trustSlides,
          minSlides: sharedVariables.trustSlides,
          moveSlides: sharedVariables.trustSlides,
          controls: false,
          slideWidth: 192,
          slideMargin: 5
        });
        setSliderControls(plugins.trustSlider, '.i13m-left-button.trust', '.i13m-right-button.trust');
      }
    }
    $('.bx-prev, .bx-next').removeAttr('href');
  }

  function onReady () {
    getScreenVariables();
    getSharedVariables();
    renderSections();
    initPlugins();
    addSliderResizer();
    setupSharedWatchers();
  }

  function parseClioProcessesData () {
    sharedVariables.clioProcessesData = sharedVariables.clioProcessesData || {};
    $('.i13m-clio-processes')
      .find('.i13m-data')
      .find('tr')
      .each(function (index) {
        sharedVariables.clioProcessesData[index] = {
          name: $(this).children().first().html(),
          text: $(this).children().last().html()
        };
      });
  }

  function renderClioProcessesControls () {
    var $buttons = $('.i13m-clio-processes').find('.i13m-clio-processes-buttons');
    _.each(sharedVariables.clioProcessesData, function (entry) {
      $buttons.append('<a>' + entry.name + '</a>');
    });
  }

  function renderClioProcesses () {
    if (sharedVariables.subPage !== 'clio') {
      return;
    }
    parseClioProcessesData();
    renderClioProcessesControls();
    setupClioProcessesWatchers();
    chooseClioProcess(0);
  }

  function renderContact () {
    setupContactWatchers();
  }

  function renderTagetikMain () {
    setupMainWatchers();
  }

  function renderSections () {
    renderContact();
    renderTagetikMain();
    renderClioProcesses();
  }

  function setSliderControls (slider, leftSelector, rightSelector) {
    slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
    slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
    $(leftSelector).on('click', function () {
      slider.prevSliderThrottled();
      return false;
    });
    $(rightSelector).on('click', function () {
      slider.nextSliderThrottled();
      return false;
    });
  }

  function setupMainWatchers () {
    $('.i13m-tagetik-main')
      .find('.i13m-container')
      .find('a')
      .click(function () {
        $.fn.fullpage.moveTo('contact');
      });
  }

  function setupClioProcessesWatchers () {
    var $modal = $('.i13m-clio-processes-modal');
    if (sharedVariables.screenWidth > 1024) {
      $('.i13m-clio-processes')
        .find('.i13m-clio-processes-buttons')
        .children('a').click(function () {
            chooseClioProcess($(this).index());
          });
    } else {
      $('.i13m-clio-processes')
        .find('.i13m-clio-processes-buttons')
        .children('a').click(function () {
            chooseClioProcess($(this).index());
            $modal.css({ 'z-index': 199 });
            TweenMax.to($modal, 0.3, { autoAlpha: 1 });
          });
    }
  }

  function setupContactWatchers () {
    var setSuccess = setInterval(function () {
      if (!$('.i13m-services-contact-form').find('.hs-form').length) {
        return;
      } else {
        clearInterval(setSuccess);
        $('.i13m-services-contact-form')
        .find('form')
        .on('submit', function(e) {
          var areAllFieldsOK = true;
          $(this)
            .find('input[required]')
            .each(function () {
              if ($(this).attr('class').indexOf('error') > -1) {
                 areAllFieldsOK = false;
              }
            });
            if (areAllFieldsOK) {
              ga('send', 'event', sharedVariables.subPage, 'formularzKontaktowy');
            } else {
              e.preventDefault();
              return false;
            }
        });
      }
    }, 100);
  }

  function setupSharedWatchers () {
    addKeyEventWatchers();
  }

  $(document).ready(onReady);
}());
