(function () {
  'use strict';

  var plugins = {};

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
    $('.i13m-left-button.gallery').click(function () {
      $.fn.fullpage.moveSlideLeft();
    });
    $('.i13m-right-button.gallery').click(function () {
      $.fn.fullpage.moveSlideRight();
    });
  }

  function dispatchKeyEvents (event) {
    if (event.which === 37) {
      $.fn.fullpage.moveSlideLeft();
    } else if (event.which === 39) {
      $.fn.fullpage.moveSlideRight();
    } else {
      return;
    }
  }

  function initPlugins () {
  }

  function onLoad () {
    initPlugins();
  }

  function onReady () {
    addKeyEventWatchers();
  }

  $(document).ready(onReady);
  $(window).load(onLoad);
}());
