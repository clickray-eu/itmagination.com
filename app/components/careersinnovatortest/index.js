(function () {
  'use strict';

  var EVENT_FUNCTIONS = {
    'main': function (event) {
      if (event.which === 37) {
        plugins.latestSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.latestSlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'careers': function (event) {
      if (event.which === 37) {
        plugins.latestSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.latestSlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'latest-jobs': function (event) {
      if (event.which === 37) {
        plugins.latestSlider.prevSliderThrottled();
      } else if (event.which === 39) {
        plugins.latestSlider.nextSliderThrottled();
      } else {
        return;
      }
    },
    'job-offers': function (event) {
      if (event.which === 37) {
        $('.i13m-careers-offers-list').data('i13mPagination').moveLeft();
      } else if (event.which === 39) {
        $('.i13m-careers-offers-list').data('i13mPagination').moveRight();
      } else {
        return;
      }
    }
  };

  var sharedVariables = {
    successStories: {
      person1: {},
      person2: {},
      person3: {},
      person4: {}
    },
    successStoriesIndex: 'person1',
    successStoriesStoryIndex: 1
  };

  var plugins = {};

  function addKeyEventWatchers () {
    $(window).on('window:keydown', dispatchKeyEvents);
  }

  function addSliderResizer () {
    $(window).on('resize', function () {
      if (!!plugins.latestSlider && !!plugins.latestSlider.reloadSlider) {
        plugins.latestSlider.reloadSlider();
      }
    });
  }

  function initPlugins () {
    if($('.bxslider').length) {
      plugins.latestSlider = $('.bxslider').bxSlider({
        controls: false,
        hideControlOnEnd: true,
        infiniteLoop: true,
        maxSlides: sharedVariables.latestSlides,
        minSlides: sharedVariables.latestSlides,
        moveSlides: sharedVariables.latestSlides,
        pager: false,
        slideWidth: 180,
        slideMargin: 32
      });
      setSliderControls(plugins.latestSlider, '.i13m-left-button.latest', '.i13m-right-button.latest');
    }
    plugins.pagination = $('.i13m-careers-offers-list').i13mPagination({
      controlsSelector: '.i13m-careers-offers-controls'
    });
    plugins.paginationB = $('.bxslider-test').i13mPagination({
      controlsSelector: '.i13m-careers-offers-controls'
    });

    if (sharedVariables.recruitmentSlides && $('.bxslider-recruitment-mobile').length) {

      plugins.recruitmentSlider = $('.bxslider-recruitment-mobile').bxSlider({
        controls: false,
        maxSlides: sharedVariables.recruitmentSlides,
        minSlides: sharedVariables.recruitmentSlides,
        moveSlides: 1,
        slideWidth: 120,
        slideMargin: 10,
        infiniteLoop: false
      });
      setSliderControls(plugins.recruitmentSlider, '.i13m-left-button.recruitment', '.i13m-right-button.recruitment');
    }

    $.modal.defaults.closeClass = 'i13m-modal-close';
    $.modal.defaults.fadeDuration = 100;
  }

  function closeStoryControl () {
    var timeLine = new TimelineMax();
    timeLine
      .to($('.i13m-careers-itmw-success-content-front'), 0.6, {
        css: {
          rotationY: 0,
          zIndex: 2,
          backfaceVisibility: 'hidden'
        }
      })
      .to($('.i13m-success-control'), 0.6, {
        css: {
          rotationY: 180,
          zIndex: 1,
          backfaceVisibility: 'hidden'
        }
      }, '-=0.6');
  }

  function createAnimation (selector) {
    var timeLine = new TimelineMax();
    var mainElement = $(selector).parent().parent();
    var movement = 10;
    timeLine
      .to($(selector).find('.i13m-careers-offer-title'), 0.1, { autoAlpha: 0, display: 'none'})
      .to($(selector).find('.i13m-careers-offer-apply'), 0.1, { autoAlpha: 1, display: 'flex'}, '-=0.1')
      .to([$(selector), $(selector).find('.i13m-careers-offer-apply')], 0.1, { css: { height: '190px' }})
      .to(mainElement, 0.1, { y: '-' + movement, css: { marginTop: '6px', marginBottom: '6px' }},  '-=0.1')
      .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { height: '34px' }}, '-=0.1')
      .to(mainElement.find('.i13m-careers-offer-title.additional'), 0.1, { autoAlpha: 1, display: 'block' }, '-=0.1')
      .to(mainElement.find('.i13m-careers-offer-salary'), 0.1, { autoAlpha: 0, display: 'none' }, '-=0.1')
      .to(mainElement.find('.i13m-careers-offer-city'), 0.1, { css: { color: '#0098da' } }, '-=0.1')
      .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { backgroundColor: '#2c3e50' } }, '-=0.1');
    return timeLine;
  }

  function dispatchKeyEvents (event) {
    var section = _.last(_.last(window.location.href.split('/')).split('#'));
    var fn = EVENT_FUNCTIONS[section];
    if (!fn) {
      return;
    }
    fn(event);
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();

    sharedVariables.screenWidth = $(window).width();

    if (sharedVariables.screenWidth <= 767) {
      sharedVariables.latestSlides = 1;
      sharedVariables.recruitmentSlides = 2;
    } else if (sharedVariables.screenWidth <= 1024) {
      sharedVariables.latestSlides = 2;
      sharedVariables.recruitmentSlides = 2;
    } else {
      sharedVariables.latestSlides = 4;
    }
  }

  function moveStoryCarouselLeft () {
    if (sharedVariables.successStoriesStoryIndex === 1) {
      return;
    }
    sharedVariables.successStoriesStoryIndex -= 1;
    selectStory();
  }

  function moveStoryCarouselRight () {
    if (sharedVariables.successStoriesStoryIndex >= $('.i13m-success-years-container').children().size()) {
      return;
    }
    sharedVariables.successStoriesStoryIndex += 1;
    selectStory();
  }

  function openStoryControl () {
    var timeLine = new TimelineMax();
    timeLine
      .to($('.i13m-success-control'), 0.6, {
        css: {
          rotationY: 360,
          zIndex: 2
        }
      })
      .to($('.i13m-careers-itmw-success-content-front'), 0.6, {
        css: {
          rotationY: 180,
          zIndex: 1
        }
      }, '-=0.6');
  }

  function parseStories () {
    var table = $('.i13m-career-success-table');

    table
      .children()
      .children()
      .each(function () {
        $(this)
          .children()
          .each(function () {
            var $this = $(this);
            var person = _.first($this.attr('class').split(' '));
            var value = $this.html();
            var year;
            if (value === '&nbsp;') {
              return;
            }
            if ($this.hasClass('name')) {
              sharedVariables.successStories[person].name = value;
            }
            if ($this.hasClass('post')) {
              sharedVariables.successStories[person].post = value;
            }
            if ($this.hasClass('year')) {
              year = $this.attr('class').split(' ')[1];
              sharedVariables.successStories[person].stories = sharedVariables.successStories[person].stories || { years: {}, texts: {} };
              sharedVariables.successStories[person].stories.years[year] = value;
            }
            if ($this.hasClass('text')) {
              year = $this.attr('class').split(' ')[1];
              sharedVariables.successStories[person].stories = sharedVariables.successStories[person].stories || { years: {}, texts: {} };
              sharedVariables.successStories[person].stories.texts[year] = value;
            }
          });
      });
  }

  function renderStory () {
    var story = sharedVariables.successStories[sharedVariables.successStoriesIndex];
    var $person = $('.i13m-success-graphic-person');
    var $yearContainer = $('.i13m-success-years-container');
    var $textContainer = $('.i13m-success-text-container');

    $person
      .children('.i13m-success-graphic-name')
      .html(story.name);

    $person
      .children('.i13m-success-graphic-post')
      .html(story.post);

    $yearContainer.empty();
    $textContainer.empty();
    _.each(story.stories.years, function(year, key) {
      var mobileYear = sharedVariables.screenWidth < 768 ? '<p style="width:100%;text-align:center;font-weight:bold;margin:0 0 -.5em;">' + year + '</p><br>' : '';
      $yearContainer.append('<div class="i13m-success-year-container" data-year="' + key + '"><div class="i13m-success-year">' + year + '</div><div class="i13m-success-dot"></div></div>');
      $textContainer.append('<div class="' + key + '">' + mobileYear + story.stories.texts[key] + '</div>');
    });
    $('.i13m-success-year-container').click(selectStory);
    sharedVariables.successStoriesStoryIndex = 1;
    selectStory();
  }

  function renderStoryControl () {
    $('.i13m-success-control')
      .children('.i13m-success-control-person')
      .each(function () {
        var $this = $(this);
        var person = _.last($this.attr('class').split(' '));
        var story = sharedVariables.successStories[person];

        $this
          .find('h5')
          .html(story.name);

        $this
          .find('h6')
          .html(story.post);
      });
  }

  function selectStory () {
    var $this = $(this);
    var yearIndex = $this.data('year') || 'year' + sharedVariables.successStoriesStoryIndex;
    sharedVariables.successStoriesStoryIndex = parseInt(_.last(yearIndex));
    $('.i13m-success-year-container')
      .each(function () {
        $(this).removeClass('active');
      })
      .filter(function () {
        return $(this).data('year') === yearIndex;
      })
      .addClass('active');
    $('.i13m-success-text-container')
      .children()
      .removeClass('active')
      .filter(function () {
        return $(this).attr('class') === yearIndex;
      })
      .addClass('active');
  }

  function setSliderControls (slider, leftSelector, rightSelector) {
    slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
    slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
    $(leftSelector).on('click', function () {
      slider.prevSliderThrottled();
      return false;
    });
    $(rightSelector).on('click', function () {
      slider.nextSliderThrottled();
      return false;
    });
  }

  function setupH2Animation () {
    var timeLine = new TimelineMax({
      onComplete: function() {
        this.restart();
      }
    });
    var textDelay = 8;
    $('.i13m-careers-search-content')
      .children('h2')
      .children('p')
      .sort(function () {
        return Math.random() > 0.5 ? 1 : -1;
      })
      .each(function () {
        timeLine
          .to($(this), 0.3, { autoAlpha: 1, display: 'inline' })
          .to($(this), 0.3, { autoAlpha: 0, display: 'none' }, '+=' + textDelay);
      });
  }

  function setupStories () {
    parseStories();
    renderStoryControl();
    renderStory();
    setupStoryWatchers();
  }

  function setupStoryWatchers() {
    $('.i13m-success-control-person').click(function () {
      var $this = $(this);
      var person = _.last($this.attr('class').split(' '));
      sharedVariables.successStoriesIndex = person;
      renderStory();
      closeStoryControl();
    });

    $('.i13m-success-control-close').click(closeStoryControl);
    $('.i13m-success-more').click(openStoryControl);
    $('.i13m-left-button.success').click(moveStoryCarouselLeft);
    $('.i13m-right-button.success').click(moveStoryCarouselRight);
  }

  function setHoverWatchers () {
    $('.i13m-careers-offer-link')
      .each(function () {
          $(this).hover(function () {
            if (!this.animation) {
              this.animation = createAnimation(this);
            } else {
              this.animation.play().timeScale(1);
            }
        }, function () {
          if (!this.animation) {
            this.animation = createAnimation(this);
          } else {
            this.animation.reverse().timeScale(4);
          }
        });
      });
  }

  function showSliderOffers () {
    $('.bxslider')
      .find('.i13m-careers-offer')
      .css({ display: 'block'});
  }

  $(document).ready(function () {
    getScreenVariables();
    initPlugins();
    setHoverWatchers();
    showSliderOffers();
    setupH2Animation();
    setupStories();
    addSliderResizer();
    addKeyEventWatchers();
  });
}());
