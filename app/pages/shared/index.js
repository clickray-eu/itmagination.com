(function () {
  'use strict';

  var manualScrollDuration = 400;
  var automaticScrollDuration = 1500;

  var FULLPAGE_DEFAULTS = {
    animateAnchor: true,
    headerOffset: 0,
    controlArrows: false,
    scrollingSpeed: automaticScrollDuration,
    easing: 'easeInOutCubic',
    fitToSectionDelay: 500,
    responsiveWidth: 1024,
    responsiveHeight: 720,

    afterLoad: function (anchorLink, index) {
      updateLanguageLinks(anchorLink);
      applyPageSpecificStyles(index);
      fireAnimation(index);
    },
    onLeave: function (index, nextIndex) {
      manualCareersSelect(nextIndex);
      processScrollDown(nextIndex);
      if ($.modal) {
        $.modal.close();
      }
    }
  };

  var SECTION_TO_REMOVE = {
    innovations: {
      clio: [
        'architecture'
      ],
      tagetik: [
        'choose'
      ]
    },
    // careers: [
    //   'itmw-success-widget'
    // ],
    rebranding: [
      'colors'
    ]
  };

  var SPECIFIC_FULLPAGE_SETTINGS = {
    about: {
      headerOffset: 72,
      normalScrollElements: '.i13m-home-board-modal, .i13m-about-policy-modal',
      onLeave: function (index, nextIndex, direction) {
        FULLPAGE_DEFAULTS.onLeave(index, nextIndex);
        return _.throttle(function (index, nextIndex, direction) {
          var directionNumber = direction === 'up' ? -1 : 1;
          var bxPager;
          var slideIndex;
          var destination;
          if (index === 5) {
            bxPager = $('.i13m-about-values-container').find('.bx-pager');
            slideIndex = bxPager.find('.active').data('slide-index');
            destination = slideIndex + directionNumber;
            if (destination >= 0 && destination <= 2 && nextIndex > 1) {
              bxPager
              .children()
              .eq(destination)
              .find('a')
              .click();
              $.fn.fullpage.setAllowScrolling(false);
              $.fn.fullpage.setKeyboardScrolling(false);
              setTimeout(function () {
                $.fn.fullpage.setAllowScrolling(true);
                $.fn.fullpage.setKeyboardScrolling(true);
              }, 500);
              return false;
            }
          }
        }, {
          leading: true,
          trailing: false
        }, 500)(index, nextIndex, direction);
      }
    },
    booster: {
      headerOffset: 72,
    },
    careers: {
      headerOffset: 72,
    },
    contact: {
      headerOffset: 72,
      normalScrollElements: '.i13m-contact-form',
      onLeave: function () {
        $('.i13m-map-tooltip.poland').tooltipster('close');
      }
    },
    home: {
      headerOffset: 72,
      normalScrollElements: '.i13m-home-portfolio-modal',
      onLeave: function () {
        $('.i13m-map-tooltip.poland').tooltipster('close');
      }
    },
    homeinnovatortest: {
      headerOffset: 72,
    },
    innovations: {
      headerOffset: 72,
    },
    inlab: {
      autoScrolling: false,
      fitToSection: false,
      headerOffset: 72
    },
    inlabitm: {
      autoScrolling: false,
      fitToSection: false,
      headerOffset: 72
    },
    rebranding: {
      headerOffset: 72,
      onLeave: function (index, nextIndex, direction) {
        FULLPAGE_DEFAULTS.onLeave(index, nextIndex);
        return _.throttle(function (index, nextIndex, direction) {
          var directionNumber = direction === 'up' ? -1 : 1;
          var bxPager;
          var slideIndex;
          var destination;
          if (index === 4) {
            bxPager = $('.i13m-rebranding-promise').find('.bx-pager');
            slideIndex = bxPager.find('.active').data('slide-index');
            destination = slideIndex + directionNumber;
            if (destination >= 0 && destination <= 2 && nextIndex > 1) {
              bxPager
              .children()
              .eq(destination)
              .find('a')
              .click();
              $.fn.fullpage.setAllowScrolling(false);
              $.fn.fullpage.setKeyboardScrolling(false);
              setTimeout(function () {
                $.fn.fullpage.setAllowScrolling(true);
                $.fn.fullpage.setKeyboardScrolling(true);
              }, 500);
              return false;
            }
          }
          if (index === 5) {
            bxPager = $('.i13m-rebranding-symbology').find('.bx-pager');
            slideIndex = bxPager.find('.active').data('slide-index');
            destination = slideIndex + directionNumber;
            if (destination >= 0 && destination <= 2 && nextIndex > 1) {
              bxPager
              .children()
              .eq(destination)
              .find('a')
              .click();
              $.fn.fullpage.setAllowScrolling(false);
              $.fn.fullpage.setKeyboardScrolling(false);
              setTimeout(function () {
                $.fn.fullpage.setAllowScrolling(true);
                $.fn.fullpage.setKeyboardScrolling(true);
              }, 500);
              return false;
            }
          }
          if (index === 8) {
            bxPager = $('.i13m-rebranding-culture').find('.bx-pager');
            slideIndex = bxPager.find('.active').data('slide-index');
            destination = slideIndex + directionNumber;
            if (destination >= 0 && destination <= 2 && nextIndex > 1) {
              bxPager
              .children()
              .eq(destination)
              .find('a')
              .click();
              $.fn.fullpage.setAllowScrolling(false);
              $.fn.fullpage.setKeyboardScrolling(false);
              setTimeout(function () {
                $.fn.fullpage.setAllowScrolling(true);
                $.fn.fullpage.setKeyboardScrolling(true);
              }, 500);
              return false;
            }
          }
        }, {
          leading: true,
          trailing: false
        }, 500)(index, nextIndex, direction);
      }
    },
    joinus: {
      autoScrolling: false,
      fitToSection: false, 
      headerOffset: 72
    },
    services: {
      headerOffset: 72
    },
  };

  var FIX_HEADER_FOR = {
    booster: true,
    jobs: true,
    joinus: true
  };

  var RAISE_SCROLLDOWN_FOR = {
    services: true,
    innovations: true
  };

  var ANIMATION_SETTINGS = {
    about: {
      5: function () {
        $(window).trigger('resize');
      }
    },
    careers: {      
      7: function () {
        var timeLine = new TimelineMax();
        timeLine
          .to($('.i13m-careers-recruitment-text-title'), 0.3, { autoAlpha: 1 })
          .to($('.i13m-careers-recruitment-text-subtitle'), 0.3, { autoAlpha: 1 });

        $('.i13m-careers-recruitment-animation-elements')
          .children()
          .each(function () {
            timeLine.to($(this), 0.1, { autoAlpha: 1 }, '-=0.05');
          });

        $('.i13m-careers-recruitment-animation-texts')
          .children('td[colspan="3"]')
          .each(function () {
            timeLine.to($(this), 0.2, { autoAlpha: 1 }, '-=0.2');
          });

        timeLine.to($('.i13m-careers-recruitment-footer'), 0.3, { autoAlpha: 1 });
      },
      // 4: function () {
      //   var timeLine = new TimelineMax();
      //   $('.i13m-careers-itmw-facts-table-data')
      //     .children()
      //     .each(function (index) {
      //       var $el = $(this);
      //       var latency = !!index ? '-=0.9' : undefined;
      //       var numberObj = {
      //         number: 0
      //       };
      //       var number = animationData.careers.facts[index];
      //       timeLine.to(numberObj, 1, {
      //         number: number,
      //         roundProps: 'number',
      //         onUpdate: function () {
      //           $el.text(Math.round(numberObj.number));
      //         }
      //       }, latency);
      //     });
      // },
      // 8: function () {
      //   var timeLine = new TimelineMax();

      //   timeLine
      //     .to($('.i13m-careers-itmw-initiatives-text'), 0.3, { autoAlpha: 1 })
      //     .to($('.i13m-careers-initiatives-logo'), 0.3, { autoAlpha: 1 }, '-=0.1')
      //     .to($('.i13m-careers-initiatives-circle'), 0.3, { autoAlpha: 1 }, '-=0.1')
      //     .to($('.i13m-careers-initiatives-stroke').children('g').children('path'), 0.3, { strokeDashoffset: 0 }, '-=0.1')
      //     .to($('.i13m-careers-itmw-initiatives-dot').children('div'), 0.3, { autoAlpha: 1 }, '-=0.1')
      //     .to($('.i13m-careers-itmw-initiatives-caption'), 0.3, { autoAlpha: 1 }, '-=0.1');
      // },
      9: function () {
        var timeLine = new TimelineMax();
        var $container = $('.i13m-careers-itmw-referral-widget');
        var $text = $container.find('.i13m-text');

        timeLine
          .to($text, 0.3, { autoAlpha: 1})
          .to($('.i13m-careers-referral-story1'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-careers-referral-story2'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-careers-referral-story3'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-referral-faq-link'), 0.3, { autoAlpha: 1 }, '-=0.1');
      }
    },
    innovations: {
      tagetik: {
        2: function () {
            $('.i13m-tagetik-left-gauge').circleProgress({
              value: 0.7,
              size: 150,
              fill: '#fc0',
              emptyFill: '#fff',
              startAngle: -Math.PI / 2,
              thickness: 5
            });
            $('.i13m-tagetik-middle-gauge').circleProgress({
              value: 0.5,
              size: 150,
              fill: '#fc0',
              emptyFill: '#fff',
              startAngle: -Math.PI / 2,
              thickness: 5
            });
            $('.i13m-tagetik-right-gauge').circleProgress({
              value: 0.4,
              size: 150,
              fill: '#fc0',
              emptyFill: '#fff',
              startAngle: -Math.PI / 2,
              thickness: 5
            });
        },
        3: function () {
          var timeLine = new TimelineMax();

          timeLine.delay(0.2);

          $('.i13m-tagetik-management-elements')
            .children()
            .each(function () {
              timeLine.to($(this), 0.1, { autoAlpha: 1 }, '-=0.05');
            });

          $('.i13m-tagetik-management-texts')
            .children('td[colspan="3"]')
            .each(function () {
              timeLine.to($(this), 0.2, { autoAlpha: 1 }, '-=0.2');
            });

        },
        4: function () {
          var timeLine = new TimelineMax();

          $('.i13m-tagetik-benefits-card')
            .each(function () {
              var $this = $(this);
              timeLine.to($this, 0.3, { autoAlpha: 1 }, '-=0.15');
            });
        }
      }
    },
    services: {
      3: function () {
        $(window).trigger('resize');
      }
    }
  };

  var sharedVariables = {};
  var cookies = (function() {
    var cookieString = null;
    var cookieArray = [];

    function getValueOf (name) {
      if (newCookies()) {
          parseCookieString();
      }
      return cookieArray[name];
    }

    function newCookies () {
      return cookieString !== document.cookie;
    }

    function parseCookieString () {
      cookieString = document.cookie;

      var cookies = cookieString.split(';');

      cookieArray = [];

      for (var i in cookies) {
        var nameVal = cookies[i].split('=');
        var name = nameVal[0].trim();
        var value = nameVal[1] ? nameVal[1].trim() : '';

        cookieArray[name] = value;
      }
    }

    function setCookie (name, value) {
      var now = new Date();
      var time = now.getTime();
      now.setTime(time + 315569259747);
      if (newCookies()) {
          parseCookieString();
      }
      document.cookie = name + '=' + value + ';expires=' + now.toGMTString() + ';path=/';
    }

    return {
      get: function (name) {
        return getValueOf(name);
      },
      set: function (name, value) {
        setCookie(name, value);
      }
    };
  })();

  var animationData = {};
  var firedAnimations = {};

  var isMenuHidden = {
    main: true,
    services: true,
    innovations: true
  };

  var REVERSE_FONT_COLOR_FOR = {
    about: {
      2: true
    },
    careers: {
      1: true,
      2: true,
      3: true,
      9: true
    },
    contact: {
      1: true
    },
    home: {
      2: true
    },
    innovations: {
      2: true
    },
    rebranding: {
      4: true,
      8: true,
      10: true
    },
    services: {
      3: true
    }
  };

  var REVERSE_FONT_COLOR_CLASS = 'negative';

  function applyPageSpecificStyles (index) {
    var shouldApplyDifferentStyle = REVERSE_FONT_COLOR_FOR[sharedVariables.page] && REVERSE_FONT_COLOR_FOR[sharedVariables.page][index];
    var classToRemove = shouldApplyDifferentStyle ? '' : REVERSE_FONT_COLOR_CLASS;
    var classToAdd = shouldApplyDifferentStyle ? REVERSE_FONT_COLOR_CLASS : '';
    $('.i13m-navi')
      .children('a')
      .each(function () {
        $(this)
          .removeClass(classToRemove)
          .addClass(classToAdd);
      });
  }

  function addGlobalEventWatchers () {
    $(window).resize(function () {
      $(window).trigger('window:resize');
    });
    $(window).keydown(function (event) {
      event.type = 'window:keydown';
      $(window).trigger(event);
    });
  }

  function addMenuEventWatchers () {
    _.each(['services', 'innovations'], function (menu) {
      $('#i13m-' + menu + '-button, .i13m-' + menu + '-menu')
        .hover(function () {
          TweenMax.to($('.i13m-' + menu + '-menu'), 0.3, { autoAlpha: 1, display: 'block' });
          $('#i13m-' + menu + '-button').addClass('nav-btn-selected');
        }, function () {
          TweenMax.to($('.i13m-' + menu + '-menu'), 0.3, { autoAlpha: 0, display: 'none' });
          if (sharedVariables.page === menu) {
            return;
          }
          $('#i13m-' + menu + '-button').removeClass('nav-btn-selected');
        })
        .click(function () {
          if (isMenuHidden[menu]) {
            TweenMax.to($('.i13m-' + menu + '-menu'), 0.3, { autoAlpha: 1, display: 'block' });
            $('#i13m-' + menu + '-button').addClass('nav-btn-selected');
          } else {
            TweenMax.to($('.i13m-' + menu + '-menu'), 0.3, { autoAlpha: 0, display: 'none' });
            if (sharedVariables.page === menu) {
              return;
            }
            $('#i13m-' + menu + '-button').removeClass('nav-btn-selected');
          }
          isMenuHidden[menu] = !isMenuHidden[menu];
        });
    });
    $('.i13m-hamburger').click(function () {
      var $menu = $('.i13m-menu-container');
      var $hamburger = $('.i13m-hamburger');
      if (isMenuHidden.main) {
        $menu.css({'margin-top': '-512px'});
        TweenMax.to($menu, 0.6, { marginTop: 0 });
        $hamburger
          .addClass('open');
      } else {
        TweenMax.to($menu, 0.6, {
          marginTop: -512,
          onComplete: function () {
            $menu.css({'margin-top':'-200vh'});
          }
        });
        $hamburger
          .removeClass('open');
      }
      isMenuHidden.main = !isMenuHidden.main;
    });
  }

  function addModalWatcher () {
    $('.i13m-modal-close')
      .click(function () {
        var $this = $(this);
        var $modal = $this.parent();
        var timeLine = new TimelineMax();
        timeLine
          .to($modal, 0.3, { autoAlpha: 0 })
          .to($modal, 0.001, { zIndex: -99 });
      });
  }

  function addWatchers () {
    addGlobalEventWatchers();
    addMenuEventWatchers();
    addModalWatcher();
  }

  function processCookieAgreement () {
    var cookieAgreement = cookies.get('__hs_opt_out');
    if (!cookieAgreement) {
      showCookieInfo();
      setupCookieWatcher();
    }
  }

  function fixHeader () {
    if (FIX_HEADER_FOR[sharedVariables.page]) {
      $('#i13m-header').css({position: 'fixed'});
    }
    if (RAISE_SCROLLDOWN_FOR[sharedVariables.page]) {
      $('.i13m-scroll-down').css({bottom: '10%'});
    }
  }

  function fireAnimation (index) {
    var animationSetting;
    var animationFired;
    if (sharedVariables.subPage) {
      animationSetting = ANIMATION_SETTINGS[sharedVariables.page] &&
        ANIMATION_SETTINGS[sharedVariables.page][sharedVariables.subPage] &&
        ANIMATION_SETTINGS[sharedVariables.page][sharedVariables.subPage][index];
      animationFired = firedAnimations[sharedVariables.page + sharedVariables.subPage] && firedAnimations[sharedVariables.page + sharedVariables.subPage][index];
    } else {
      animationSetting = ANIMATION_SETTINGS[sharedVariables.page] && ANIMATION_SETTINGS[sharedVariables.page][index];
      animationFired = firedAnimations[sharedVariables.page] && firedAnimations[sharedVariables.page][index];
    }
    if (!animationSetting || animationFired) {
      return;
    } else {
      var animationObj = {};
      var key = sharedVariables.subPage ? sharedVariables.page + sharedVariables.subPage : sharedVariables.page;
      animationObj[key] = {};
      animationObj[key][index] = true;
      _.merge(firedAnimations, animationObj);
    }
    animationSetting();
  }

  function gatherData () {
    gatherCareerFactsData();
  }

  function gatherCareerFactsData () {
    $('.i13m-careers-itmw-facts-table-data')
      .children()
      .each(function (index) {
        var dataObj = {
          careers: {
            facts: {}
          }
        };
        dataObj.careers.facts[index] = parseInt($(this).text());
        _.merge(animationData, dataObj);
        $(this).text(0);
      });
  }

  function getScreenVariables () {
    sharedVariables.screenHeight = $(window).height();
    sharedVariables.screenWidth = $(window).width();
  }

  function getSharedVariables () {     
    sharedVariables.page = window.location.href.split('/')[4] 
      ? _.first(window.location.href.split('/')[4].split('#')) 
      : _.first(window.location.href.split('/')[2].split('.'));
    sharedVariables.subPage = window.location.href.split('/')[5] && _.first(window.location.href.split('/')[5].split('#'));   
    if($(window).width() >= 1024) { sharedVariables.page = 'careers'; } 
    
  }

  function manualCareersSelect (nextIndex) {
    var sections = {
      '1': 'main',
      '2': 'life',
      '3': 'values',
      '4': 'meet-our-innovators',
      '5': 'success-stories',
      '6': 'latest-jobs',
      '7': 'recruitment-process',
      '8': 'perks-and-benefits',
      '9': 'referral-program',
      '10': 'job-offers',
      '11': 'social'
    };
    var section = sections[nextIndex] || 'other';
    var classToChange = 'nav-btn-selected';
    if (sharedVariables.page !== 'careers') {
      return;
    }
    $('.i13m-menu-container')
      .find('.nav-btn')
      .removeClass(classToChange)
      .filter(function () {
        return _.last(_.last($(this).attr('href').split('/')).split('#')) === section;
      })
      .addClass(classToChange);
  }

  function processScrollDown (nextIndex) {
    if (nextIndex > 1) {
      var $scroll = $('.i13m-scroll-down');
      TweenMax.killTweensOf($scroll);
      TweenMax.to($scroll, 0.4, { autoAlpha: 0});
    }
  }

  function removeSections () {
    var sectionsToRemove = SECTION_TO_REMOVE[sharedVariables.page] && SECTION_TO_REMOVE[sharedVariables.page][sharedVariables.subPage]
      ? SECTION_TO_REMOVE[sharedVariables.page][sharedVariables.subPage]
      : SECTION_TO_REMOVE[sharedVariables.page];
    if (!sectionsToRemove || sharedVariables.screenWidth > 1024) {
      return;
    }
    _.each(sectionsToRemove, function (section) {
      $('.i13m-' + (sharedVariables.subPage || sharedVariables.page) + '-' + section).remove();
    });
  }

  function setLoaderAnimation () {
    TweenMax.staggerTo($('.i13m-loader circle'), 2, {
      attr: {
        r: 20
      },
      opacity: 0,
      repeat: -1
    }, 1);
  }

  function setupCookieWatcher () {
    var $cookieClose = $('.i13m-cookie-info').find('.i13m-close');

    $cookieClose.click(function () {
      var $cookieInfo = $('.i13m-cookie-info');
      cookies.set('__hs_opt_out', 'no');
      $cookieInfo.css({ 'z-index': -99 });
      TweenMax.to($cookieInfo, 0.3, { autoAlpha: 0 });
    });
  }

  function setUpdateWatchers () {
    $('.grid-item')
      .each(function () {
        var $this = $(this);
        $this.hover(function () {
          TweenMax.to($this.find('.i13m-update-header'), 0.2, {
            autoAlpha: 0,
            height: 0,
            padding: '0 1em 0 1em'
          });
          TweenMax.to($this.find('.i13m-update-header-alt'), 0.2, {
            autoAlpha: 1,
            height: '59.13px',
            padding: '1em 1em 0 1em'
          }, '-=0.2');
          TweenMax.to($this.find('.i13m-update-more'), 0.2, {
            autoAlpha: 1,
            height: '14px',
            padding: '0 1em 1em 1em'
          }, '-=0.2');
        }, function () {
          TweenMax.to($this.find('.i13m-update-header'), 0.2, {
            autoAlpha: 1,
            height: '59.13px',
            padding: '1em'
          });
          TweenMax.to($this.find('.i13m-update-header-alt'), 0.2, {
            autoAlpha: 0,
            height: 0,
            padding: '0 1em 0 1em'
          }, '-=0.2');
          TweenMax.to($this.find('.i13m-update-more'), 0.2, {
            autoAlpha: 0,
            height: 0,
            padding: '0 1em 0 1em'
          }, '-=0.2');
        });
      });
  }

  function setupNavi () {
    var $navi = $('.i13m-navi');
    if (['booster','jobs','rebrandingtour'].indexOf(sharedVariables.page) > -1) {
      $navi.remove();
    } else {
      $('.i13m-back-to-top').click(function () {
        $.fn.fullpage.moveTo('main');
      });
      $('.i13m-one-up').click(function () {
        $.fn.fullpage.moveSectionUp();
      });
      $('.i13m-one-down').click(function () {
        $.fn.fullpage.moveSectionDown();
      });
    }
  }

  function showCookieInfo () {
    var $cookieInfo = $('.i13m-cookie-info');
    $cookieInfo.css({ 'z-index': 9999 });
    TweenMax.to($cookieInfo, 0.3, { autoAlpha: 1 });
  }

  function updateLanguageLinks (anchorLink) {
    $('.i13m-menu-container')
      .find('.language-btn')
      .each(function () {
        var url = $(this).attr('href').split('#')[0];
        $(this).attr('href', url + '#' + anchorLink);
      });
  }

  function onReady () {
      getSharedVariables();
      getScreenVariables();
      var specificFullpageSettings = (sharedVariables.page === 'careers' && !sharedVariables.subPage) ? {} : SPECIFIC_FULLPAGE_SETTINGS[sharedVariables.page];
      var fullpageSettings = _.assign({}, FULLPAGE_DEFAULTS, specificFullpageSettings, {touchSensitivity: sharedVariables.touchSensitivity});
      var pagesToDisable = [
        'homeinnovatortest',
        'home',
        'services',
        'innovations',
        'about',
        'contact',
        'careers'
      ];
      if (pagesToDisable.indexOf(sharedVariables.page) > -1 && sharedVariables.screenWidth < 1024) {
        fullpageSettings.autoScrolling = false;
        fullpageSettings.fitToSection = false;
        $('#fullpage')
          .children('.section')
          .addClass('fp-auto-height');
      }
      processCookieAgreement();
      removeSections();
      setLoaderAnimation();
      gatherData();
      $('#fullpage').fullpage(fullpageSettings);
      

      if ($.fn.fullpage.setScrollingSpeed) {
        $.fn.fullpage.setKeyboardScrolling(false, 'left, right');
      }

      if (sharedVariables.page === 'home' || sharedVariables.page === 'about') {
        setUpdateWatchers();
      }

      addWatchers();
      fixHeader();
      setupNavi();
      setTimeout(function () {
        $(window).trigger('resize');
        if ($.fn.fullpage.setScrollingSpeed) {
          $.fn.fullpage.setScrollingSpeed(manualScrollDuration);
        }
      }, 100);
  }

  $(document).ready(onReady);

  $(window).load(function () {
    TweenMax.killTweensOf(('.i13m-loader circle'));
    TweenMax.to($('.i13m-loader'), 0.4, { autoAlpha: 0, zIndex: -999 });
   
  });



}());
