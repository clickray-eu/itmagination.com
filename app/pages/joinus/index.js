'use strict';

import TweenMax, {TimelineMax} from 'gsap';
import _ from 'lodash';
import 'store';
import '../../../node_modules/bxslider/dist/jquery.bxslider.min.js';
import '../../components/shared/index.js';
import '../../components/shared/jquery.fullPage.js';
import '../../components/shared/hotjar.js';
import '../../components/shared/ga.js';
import 'fullpage.js';
import 'jquery';
import 'jquery-modal';
import '../../../node_modules/zurb-twentytwenty/js/jquery.event.move.js';
import '../../../node_modules/zurb-twentytwenty/js/jquery.event.move';

export default function joinus() {
    (function () {
    'use strict';

    var plugins = {};
    var sharedVariables = {};

    function getScreenVariables () {
        sharedVariables.screenHeight = $(window).height();

        sharedVariables.screenWidth = $(window).width();

        if (sharedVariables.screenWidth <= 767) {
        sharedVariables.brandSlides = 1;
        sharedVariables.symbologySlides = 1;
        } else if (sharedVariables.screenWidth <= 1024) {
        sharedVariables.brandSlides = 1;
        sharedVariables.symbologySlides = 1;
        } else {
        }
    }


    function onReady () {
        getScreenVariables();
        applySection();
        onLinkedWatchers();
    }

    function applySection() {
        // $(".i13m-job-apply").on('click', function() {
        //     $(this).next(".i13m-job-apply-form").show();
        //
        //     $(".i13m-joinus-job-offers").animate({
        //        scrollTop: $(".i13m-joinus-job-offers").position().top - 60
        //    });
        //
        //    return false;
        // });

        $(".i13m-job-apply").click(function() {
        var form = $(this).next(".i13m-job-apply-form");
        form.show();

        var body = $("html, body");
        body.stop().animate({ scrollTop: $(form).offset().top - 50 }, 200);
        return false;
        });

        $(".i13m-job-apply-close").click(function() {
        $(this).parent(".i13m-job-apply-form").hide();
        });
    }

    function onLinkedWatchers() {
        $('.i13m-linkedin-btn span').click(function() {
        if(!IN.User.isAuthorized()) {
            //console.log("Not Authorized");
        } else {
            //console.log("Authorized");
        }
        IN.User.authorize(onLinkedInAuth);
        });
    }

    function onLinkedInAuth() {
        IN.API.Profile("me")
            .fields("firstName", "lastName", "emailAddress")
            .result(displayProfiles);
    }

    function displayProfiles(profiles) {
        var member = profiles.values[0];
        $("input[name='firstname']").val(member.firstName).change();
        $("input[name='lastname']").val(member.lastName).change();
        $("input[name='email']").val(member.emailAddress).change();
    }

    $(document).ready(onReady);

    }());

}