'use strict';

import 'store';
import TweenMax from 'gsap';
import _ from 'lodash';
import '../../../node_modules/bxslider/dist/jquery.bxslider.min.js';
import '../../components/shared/index.js';
import '../../components/shared/jquery.fullPage.js';
import 'fullpage.js';
import '../../components/shared/hotjar.js';
import '../../components/shared/ga.js';
import 'masonry-layout';

export default function inlabitm(){
    (function () {
    'use strict';

    // var EVENT_FUNCTIONS = {
    //   'intro': function (event) {
    //     if (event.which === 37) {
    //       plugins.introSlider.prevSliderThrottled();
    //     } else if (event.which === 39) {
    //       plugins.introSlider.nextSliderThrottled();
    //     } else {
    //       return;
    //     }
    //   }
    // };

    var FORM_TEXTS = {
        en: {
        cv: 'Click here to upload an InLab submission'
        },
        pl: {
        cv: 'Kliknij tu by przesłać zgłoszenie InLab'
        }
    };

    var CUSTOM_FIELDS = ['.hs_inlab_submission'];

    var plugins = {};
    var sharedVariables = {};

    var defaultDiacriticsRemovalMap = [
        {'base':'A', 'letters':'\u0104'},
        {'base':'C', 'letters':'\u0106'},
        {'base':'E', 'letters':'\u0118'},
        {'base':'L', 'letters':'\u0141'},
        {'base':'N', 'letters':'\u0143'},
        {'base':'O', 'letters':'\u00D3'},
        {'base':'S', 'letters':'\u015A'},
        {'base':'Z', 'letters':'\u0179\u017B'},
        {'base':'a', 'letters':'\u0105'},
        {'base':'c', 'letters':'\u0107'},
        {'base':'e', 'letters':'\u0119'},
        {'base':'l', 'letters':'\u0142'},
        {'base':'n', 'letters':'\u0144'},
        {'base':'o', 'letters':'\u00F3'},
        {'base':'s','letters':'\u015B'},
        {'base':'z','letters':'\u017A\u017C'}
    ];

    var diacriticsMap = {};
    for (var i=0; i < defaultDiacriticsRemovalMap.length; i++){
        var letters = defaultDiacriticsRemovalMap[i].letters;
        for (var j=0; j < letters.length ; j++){
        diacriticsMap[letters[j]] = defaultDiacriticsRemovalMap[i].base;
        }
    }

    // function addKeyEventWatchers () {
    //   $(window).on('window:keydown', dispatchKeyEvents);
    // }

    function checkReplacedFields () {
        $('.i13m-inlab-contact-form')
        .find(CUSTOM_FIELDS.join(', '))
        .each(function () {
            var $label = $(this)
            .children('div')
            .find('label');
            var $input = $(this)
            .children('div')
            .find('input');
            if (!$input.val()) {
            $label
                .addClass('invalid error');
            } else {
            $label
                .removeClass('invalid error');
            }
        });
    }

    // function dispatchKeyEvents (event) {
    //   var section = _.last(_.last(window.location.href.split('/')).split('#'));
    //   var fn = EVENT_FUNCTIONS[section];
    //   if (!fn) {
    //     return;
    //   }
    //   fn(event);
    // }

    function getCardTemplate (entry) {
        return '<div class="i13m-inlab-card">' +
                '<div class="i13m-inlab-card-header">' +
                '<img src="//cdn2.hubspot.net/hubfs/2422193/pictures/inlab/' + entry.image + '.jpg"/>' +
                '</div>' +
                '<div class="i13m-inlab-card-info">' +
                '<div class="i13m-inlab-card-name">' + entry.name + '</div>' +
                '<div class="i13m-inlab-card-skills">' + entry.skills + '</div>' +
                '</div>' +
            '</div>';
    }

    function getSharedVariables () {
        sharedVariables.language = window.location.href.split('/')[3];
    }

    function getScreenVariables () {
        sharedVariables.screenHeight = $(window).height();

        sharedVariables.screenWidth = $(window).width();

        if (sharedVariables.screenWidth <= 767) {
        sharedVariables.cardSlides = 1;
        sharedVariables.iterationsSlides = 1;
        sharedVariables.iterationSlides = 1;
        } else if (sharedVariables.screenWidth <= 1024) {
        sharedVariables.cardSlides = 3;
        sharedVariables.iterationsSlides = 1;
        sharedVariables.iterationSlides = 1;
        } else {
        }
    }

    function getVariables () {
        getSharedVariables();
        getScreenVariables();
    }

    function initPlugins () {
        // plugins.introSlider = $('.i13m-inlab-intro')
        //   .find('ul')
        //   .bxSlider({
        //     controls: false
        //   });
        //   setSliderControls(plugins.introSlider, '.i13m-left-button.intro', '.i13m-right-button.intro');
        if (sharedVariables.cardSlides) {
        plugins.councilSlider = $('.i13m-inlab-council-cards')
            .bxSlider({
            controls: false,
            minSlides: sharedVariables.cardSlides,
            maxSlides: sharedVariables.cardSlides,
            moveSlides: sharedVariables.cardSlides,
            slideWidth: 200
            });
        setSliderControls(plugins.councilSlider, '.i13m-left-button.council', '.i13m-right-button.council');
        plugins.mentorsSlider = $('.i13m-inlab-mentors-cards')
            .bxSlider({
            controls: false,
            minSlides: sharedVariables.cardSlides,
            maxSlides: sharedVariables.cardSlides,
            moveSlides: sharedVariables.cardSlides,
            slideWidth: 200
            });
        setSliderControls(plugins.mentorsSlider, '.i13m-left-button.mentors', '.i13m-right-button.mentors');
        }
        if (sharedVariables.iterationsSlides) {
        plugins.iterationsSlider = $('.i13m-inlab-iterations-container')
            .bxSlider({
            controls: false,
            infiniteLoop: false,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: sharedVariables.iterationsSlides,
            slideWidth: 192
            });
        setSliderControls(plugins.iterationsSlider, '.i13m-left-button.iterations', '.i13m-right-button.iterations');
        plugins.iterationsTextSlider = $('.i13m-inlab-iterations-text')
            .bxSlider({
            controls: false,
            pager: false,
            infiniteLoop: false
            });
        setSliderControls(plugins.iterationsTextSlider, '.i13m-left-button.iterations-text', '.i13m-right-button.iterations-text');
        }
        if (sharedVariables.iterationSlides) {
        plugins.iterationSlider = $('.i13m-inlab-iteration-container')
            .find('.i13m-inlab-iteration-q')
            .bxSlider({
            controls: false,
            infiniteLoop: false,
            minSlides: 2,
            maxSlides: 2,
            moveSlides: sharedVariables.iterationSlides,
            slideWidth: 128
            });
        setSliderControls(plugins.iterationSlider, '.i13m-left-button.iteration', '.i13m-right-button.iteration');
        }
    }

    function onReady () {
        getVariables();
        renderSections();
        initPlugins();
        teamAccordion();
        // addKeyEventWatchers();
        replaceFormElements();
    }

    function parseData (selector) {
        var data = {};
        $(selector)
        .find('tr')
        .each(function (index) {
            data[index] = {
            name: $(this).children().first().html(),
            image: removeDiacritics($(this).children().first().html()).toLowerCase().replace(/[\s]/gi, '-'),
            skills: $(this).children().last().html()
            };
        });
        return data;
    }

    function parseCouncilData () {
        sharedVariables.councilData = parseData('.i13m-inlab-council-data');
    }

    function parseMentorsData () {
        sharedVariables.mentorsData = parseData('.i13m-inlab-mentors-data');
    }

    function removeDiacritics (str) {
        return str.replace(/[^\u0000-\u007E]/g, function(a){
        return diacriticsMap[a] || a;
        });
    }

    function renderCardElements (data, container) {
        var $container = $(container);
        _.each(data, function (entry) {
        var cardTemplate = getCardTemplate(entry);
        $container.append(cardTemplate);
        });
    }

    function renderCouncil () {
        parseCouncilData();
        renderCouncilElements();
    }

    function renderCouncilElements () {
        renderCardElements(sharedVariables.councilData, '.i13m-inlab-council-cards');
    }

    function renderMentors () {
        parseMentorsData();
        renderMentorsElements();
    }

    function renderMentorsElements () {
        renderCardElements(sharedVariables.mentorsData, '.i13m-inlab-mentors-cards');
    }

    function renderSections () {
        renderCouncil();
        roundHistory();
        renderMentors();
    }

    function replaceFormElements () {
        var text = FORM_TEXTS[sharedVariables.language].cv;
        var $input;
        var id;
        var newElement;
        var getId = setInterval(function () {
        $input = $('.hs_inlab_submission')
            .children('.input')
            .children('input')
            .first();
        id = $input.attr('id');
        if (!id) {
            return;
        } else {
            clearInterval(getId);
            newElement = '<label class=\'i13m-inlab-sub-label\' for=' + id + '>' + text + '</label>';
            $('.hs_inlab_submission')
            .children('.input')
            .first()
            .append(newElement);
            $input
            .change(function () {
                var file = _.last($(this).val().split('\\')) || text;
                $('.hs_inlab_submission')
                .find('.i13m-inlab-sub-label')
                .html(file);
                checkReplacedFields();
            });
            setSubmitWatcher();
        }
        }, 100);
    }

    function setSliderControls (slider, leftSelector, rightSelector) {
        slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
        slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
        $(leftSelector).on('click', function () {
        slider.prevSliderThrottled();
        return false;
        });
        $(rightSelector).on('click', function () {
        slider.nextSliderThrottled();
        return false;
        });
    }

    function setSubmitWatcher () {
        $('.i13m-inlab-contact-form')
        .find('form')
        .on('submit', function (e) {
            checkReplacedFields();
            var areAllFieldsOK = true;
            var $form = $(this);
            $form
            .find('input[required]')
            .each(function () {
                var $input = $(this);
                if ($input.attr('class').indexOf('error') > -1) {
                areAllFieldsOK = false;
                }
            });
            if (areAllFieldsOK) {
            console.log('ok');
            } else {
            console.log('nok');
            e.preventDefault();
            return false;
            }
        });
    }

    function roundHistory () {
        setupRoundWatchers();
    }

    function setupRoundWatchers() {
        $('.i13m-round-control-close').click(closeRoundControl);
        $('.i13m-round-more').click(openRoundControl);
    }

    function closeRoundControl () {
        var timeLine = new TimelineMax();
        timeLine
        .to($('.i13m-content.round-iteration-front'), 0.6, {
            css: {
            rotationY: 0,
            zIndex: 2,
            backfaceVisibility: 'hidden'
            }
        })
        .to($('.i13m-success-control'), 0.6, {
            css: {
            rotationY: 180,
            zIndex: 1,
            backfaceVisibility: 'hidden'
            }
        }, '-=0.6');
    }

    function openRoundControl () {
        var timeLine = new TimelineMax();
        timeLine
        .to($('.i13m-success-control'), 0.6, {
            css: {
            rotationY: 360,
            zIndex: 2
            }
        })
        .to($('.i13m-content.round-iteration-front'), 0.6, {
            css: {
            rotationY: 180,
            zIndex: 1
            }
        }, '-=0.6');
    }

    function teamAccordion() {
        var animTime = 300,
            clickPolice = false;

        $(document).on('touchstart click', '.i13m-btn-team', function(){
            if(!clickPolice){
            clickPolice = true;

            var currIndex = $(this).index('.i13m-btn-team'),
                targetHeight = $('.i13m-content-team-inner').eq(currIndex).outerHeight();

            $('.i13m-btn-team').removeClass('selected');
            $(this).addClass('selected');

            $('.i13m-content-team').stop().animate({ height: 0 }, animTime);
            $('.i13m-content-team').eq(currIndex).stop().animate({ height: targetHeight }, animTime);

            setTimeout(function(){ clickPolice = false; }, animTime);
            }

        });
    }

    $(document).ready(onReady);
    }());
}