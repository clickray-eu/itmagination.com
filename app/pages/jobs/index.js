'use strict';

import TweenMax, {TimelineMax} from 'gsap';
import _ from 'lodash';
import 'store';
import '../../../node_modules/bxslider/dist/jquery.bxslider.min.js';
import '../../components/shared/index.js';
import '../../components/shared/hotjar.js';
import '../../components/shared/ga.js';
import 'fullpage.js';
import 'jquery';
import 'jquery-modal';
import './tooltipster';

export default function jobs(){

    (function () {
    'use strict';

    var sharedVariables = {};
    var plugins = {};

    var FORM_TEXTS = {
        en: {
        cv: 'Click here to upload CV'
        },
        pl: {
        cv: 'Kliknij tu by przesłać CV'
        }
    };

    var CUSTOM_FIELDS = ['.hs_cv'];

    function addLinkWatchers () {
        var width = window.innerWidth / 3;
        var height = window.innerHeight / 3;
        $('.share')
        .children('a')
        .each(function () {
            $(this).click(function () {
            window.open(this.href, 'newwindow', 'width=' + width + ', height=' + height + ', top=' + height + ', left=' + width);
            return false;
            });
        });
    }

    function checkReplacedFields () {
        $('.i13m-job-apply-form')
        .find(CUSTOM_FIELDS.join(', '))
        .each(function () {
            var $label = $(this)
            .children('div')
            .find('label');
            var $input = $(this)
            .children('div')
            .find('input');
            if (!$input.val()) {
            $label
                .addClass('invalid error');
            } else {
            $label
                .removeClass('invalid error');
            }
        });
    }

    function createAnimation (selector) {
        var timeLine = new TimelineMax();
        var mainElement = $(selector).parent().parent();
        var movement = 10;
        timeLine
        .to($(selector).find('.i13m-careers-offer-title'), 0.1, { autoAlpha: 0, display: 'none'})
        .to($(selector).find('.i13m-careers-offer-apply'), 0.1, { autoAlpha: 1, display: 'flex'}, '-=0.1')
        .to([$(selector), $(selector).find('.i13m-careers-offer-apply')], 0.1, { css: { height: '190px' }})
        .to(mainElement, 0.1, { y: '-' + movement, css: { marginTop: '4px', marginBottom: '0px' } }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { height: '34px' } }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-title.additional'), 0.1, { autoAlpha: 1, display: 'block' }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-salary'), 0.1, { autoAlpha: 0, display: 'none' }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-city'), 0.1, { css: { color: '#0098da' } }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { backgroundColor: '#2c3e50' } }, '-=0.1');
        return timeLine;
    }

    function getScreenVariables () {
        sharedVariables.screenHeight = $(window).height();
        sharedVariables.screenWidth = $(window).width();

        if (sharedVariables.screenWidth <= 767) {
        sharedVariables.moreSlider = 1;
        } else if (sharedVariables.screenWidth <= 1024) {
        sharedVariables.moreSlider = 1;
        } else {
        }
    }

    function fillHiddenFields() {
        $('.i13m-job-info')
        .children()
        .each(function () {
            var $el = $(this);
            var type = $el.attr('class');
            var value = $el.html().trim();
            $('input[name="' + type + '"]')
            .each(function () {
                var $this = $(this);
                if (type === 'reference_number') {
                value = value || 'default';
                $this.val($this.val().trim() + value + ';');
                } else {
                $this.val(value);
                }
            });
        });
        $('.i13m-job-recommend-form.individual')
        .find('input[name="reference_number"]')
        .each(function () {
            var $el = $(this);
            $el.val($el.val().trim() + 'default;');
        });
    }

    function getSharedVariables () {
        sharedVariables.language = window.location.href.split('/')[3].substr(0, 2);
    }

    function initPlugins () {
        $('.i13m-social-tooltip')
        .tooltipster({
            animationDuration: 150,
            interactive: true,
            side: 'bottom',
            trigger: 'custom',
            maxWidth: 100,
            triggerOpen: {
                click: true,
                mouseenter: true,
                tap: true
            },
            triggerClose: {
                click: true,
                scroll: true,
                mouseleave: true,
                tap: true
            }
        });
        if (sharedVariables.moreSlider) {
        plugins.moreSlider = $('.i13m-job-more-container').bxSlider({
            maxSlides: 1,
            minSlides: 1,
            moveSlides: 1,
            controls: false,
            slideWidth: 180,
            slideMargin: 10
        });
        setSliderControls(plugins.moreSlider, '.i13m-left-button.more', '.i13m-right-button.more');
        }
    }

    function replaceFormElements () {
        var text = FORM_TEXTS[sharedVariables.language].cv;
        var $cv_input;
        var $ref_input;
        var cv_id;
        var ref_id;
        var newElement;
        var getId = setInterval(function () {
        $cv_input = $('.hs_cv')
            .children('.input')
            .children('input')
            .first();
        cv_id = $cv_input.attr('id');
        $ref_input = $('.hs_referrer_email')
            .children('.input')
            .children('input')
            .first();
        ref_id = $ref_input.attr('data-reactid');
        if (!(cv_id || ref_id) || !$('input[name="referrer_email"]').attr('class')) {
            return;
        } else {
            clearInterval(getId);
            newElement = '<label class=\'i13m-jobs-cv-label\' for=' + cv_id + '>' + text + '</label>';
            $('.hs_cv')
            .children('.input')
            .first()
            .append(newElement);
            if (cv_id) {
            $cv_input.change(function () {
                var file = _.last($(this).val().split('\\')) || text;
                $('.hs_cv')
                .find('.i13m-jobs-cv-label')
                .html(file);
                checkReplacedFields();
            });
            }
            setSubmitWatcher();
            fillHiddenFields();
        }
        }, 100);
    }

    function selectCareers () {
        var classToAdd = 'nav-btn-selected';
        $('.i13m-menu-container')
        .children()
        .children()
        .filter(function () {
            return $(this).attr('href') && $(this).attr('href').indexOf('careers') > -1;
        })
        .addClass(classToAdd);
    }

    function setFormButtons () {
        var applyClass = '.i13m-job-apply-form';
        var recommendClass = '.i13m-job-recommend-form';
        $('.i13m-job-apply')
        .click(function () {       
            TweenMax.to($(applyClass), 0.3, { opacity: 1, zIndex: 99 });
            TweenMax.to(window, 1, { scrollTo: 400 });
        });
        $('.i13m-job-apply-close')
        .click(function () {
            TweenMax.to($(applyClass), 0.3, { opacity: 0, zIndex: -99 });
        });
        $('.i13m-job-recommend')
        .click(function () {
            TweenMax.to($(recommendClass), 0.3, { opacity: 1, zIndex: 99 });
            TweenMax.to(window, 1, { scrollTo: 400 });
        });
        $('.i13m-job-recommend-close')
        .click(function () {
            TweenMax.to($(recommendClass), 0.3, { opacity: 0, zIndex: -99 });
        });
    }

    function setHoverWatchers () {
        $('.i13m-careers-offer-link')
        .each(function () {
            $(this).hover(function () {
            if (!this.animation) {
                this.animation = createAnimation(this);
            } else {
                this.animation.play().timeScale(1);
            }
            }, function () {
            if (!this.animation) {
                this.animation = createAnimation(this);
            } else {
                this.animation.reverse().timeScale(4);
            }
            });
        });
    }

    function setSliderControls (slider, leftSelector, rightSelector) {
        slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
        slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
        $(leftSelector).on('click', function () {
        slider.prevSliderThrottled();
        return false;
        });
        $(rightSelector).on('click', function () {
        slider.nextSliderThrottled();
        return false;
        });
    }

    function setSubmitWatcher () {
        $('.i13m-job-apply-form')
        .find('.hs_submit')
        .find('input')
        .click(checkReplacedFields);
        $('.i13m-job-apply-form')
        .find('form')
        .each(function () {
            $(this)
            .on('submit', function(e) {
                var areAllFieldsOK = true;
                var $form = $(this);
                $form
                .find('input[required]')
                .each(function () {
                    var $input = $(this);
                    if ($input.attr('class').indexOf('error') > -1) {
                    areAllFieldsOK = false;
                    }
                });
                if (areAllFieldsOK) {
                } else {
                e.preventDefault();
                return false;
                }
            });
            });
        $('.i13m-job-recommend-form')
        .find('.hs_submit')
        .find('input')
        .click(checkReplacedFields);
        $('.i13m-job-apply-form')
        .find('form')
        .each(function () {
            $(this)
            .on('submit', function(e) {
                var areAllFieldsOK = true;
                var $form = $(this);
                $form
                .find('input[required]')
                .each(function () {
                    var $input = $(this);
                    if ($input.attr('class').indexOf('error') > -1) {
                    areAllFieldsOK = false;
                    }
                });
                if (areAllFieldsOK) {
                } else {
                e.preventDefault();
                return false;
                }
            });
            });
    }

    function onLinkedWatchers() {
        $('.i13m-linkedin-btn span').click(function() {
        if(!IN.User.isAuthorized()) {
            //console.log("Not Authorized");
        } else {
            //console.log("Authorized");
        }
        IN.User.authorize(onLinkedInAuth);
        });
    }

    function onLinkedInAuth() {
        IN.API.Profile("me")
            .fields("firstName", "lastName", "emailAddress")
            .result(displayProfiles);
    }

    function displayProfiles(profiles) {
        var member = profiles.values[0];
        $("input[name='firstname']").val(member.firstName).change();
        $("input[name='lastname']").val(member.lastName).change();
        $("input[name='email']").val(member.emailAddress).change();
    }

    $(document).ready(function () {
        getScreenVariables();
        initPlugins();
        getSharedVariables();
        selectCareers();
        replaceFormElements();
        setHoverWatchers();
        setFormButtons();
        addLinkWatchers();
        onLinkedWatchers();
    });
    }());
}