'use strict';

import Fuse from 'fuse.js';
import TweenMax from 'gsap';
import _ from 'lodash';
import 'store';
import '../../../node_modules/bxslider/dist/jquery.bxslider.min.js';
import '../../components/shared/index.js';
import '../../components/shared/jquery.fullPage.js';
import '../../components/shared/hotjar.js';
import '../../components/shared/ga.js';
import 'fullpage.js';
import 'awesomplete';
import 'jquery';
import 'jquery-modal';

import i13mPagination from './pagination';
import tooltipster from './tooltipster';
import widget from './widget';

i13mPagination($);

export default function homepage(){
  (function () {
    'use strict';

    var transform = (function() {
      var vendors = ['webkit', 'moz', 'ms'];
      var style   = document.createElement( 'div' ).style;
      var trans   = 'transform' in style ? 'transform' : undefined;

      for( var i = 0, count = vendors.length; i < count; i++ ) {
        var prop = vendors[i] + 'Transform';
        if( prop in style ) {
          trans = prop;
          break;
        }
      }

      return trans;
    })();

    var CAREERS_URL = '2422193.hs-sites.com';

    var textElements = $('.i13m-widget-text-container').children();
    var upperStroke = $('.i13m-widget-upper-stroke').children().children().children('path')[0];
    var input = $('#i13m-widget-input');
    var inputStroke = $('.i13m-widget-input-container').children().children().children('path')[0];
    var lowerStroke = $('.i13m-widget-lower-stroke').children().children().children('path')[0];
    var imagine = $('.i13m-widget-imagine');
    var done = $('.i13m-widget-animation');
    var link = $('.i13m-widget-link');
    var carousel = $('.i13m-widget-carousel-container').children().children();

    var previousInputValue;

    var plugins = {};
    var sharedVariables = {};

    var tooltips = [];
    var IMAGE_WIDTH = 1128;
    var IMAGE_HEIGHT = 557;
    var IMAGE_ASPECT_RATIO = IMAGE_WIDTH / IMAGE_HEIGHT;
    var TOOLTIP_SIZE = 0.01;
    var padding = 0.01;

    var WIDGET_TEXTS = {
      'software development': 'the custom software you need.',
      'cloud': 'your business in the cloud.',
      'data science': 'the insights you need for growth.',
      'data management': 'using your data for competitive advantage.',
      'performance management': 'financial performance intelligence you need in one application.',
      'managed services': 'comprehensive maintenance services for your company.',
      'outsourcing': 'talents you need.',
      'erp': 'effective data exchange optimization in your company.'
    };

    var WIDGET_QUERY_TEXTS = [
      {
        name: 'software development',
        text: 'user experience custom software development mobile applications ' +
          'development application lifecycle management architecture and infrastructure ' +
          'application migration application integration application maintenane ' +
          'quality assurance',
        url: 'software-development'
      },
      {
        name: 'cloud',
        text: 'cloud strategy cloud transformation cloud managed services',
        url: 'cloud'
      },
      {
        name: 'data science',
        text: 'big data',
        url: 'data-science'
      },
      {
        name: 'data management',
        text: 'information strategy business intelligence data warehousing data lakes data ' +
          'governance and quality migration and integration fraud and risk management',
        url: 'data-management'
      },
      {
        name: 'performance management',
        text: 'tagetik',
        url: 'performance-management'
      },
      {
        name: 'managed services',
        text: 'team managed services application managed services infrastructure managed ' +
          'services cloud managed services service desk',
        url: 'managed-services'
      },
      {
        name: 'outsourcing',
        text: 'staff augmentation team leasing time and material model',
        url: 'outsourcing'
      },
      {
        name: 'erp',
        text: 'microsoft dynamics ax',
        url: 'erp'
      }
    ];

    var EVENT_FUNCTIONS = {
      'main': function (event) {
        if (event.which === 13) {
          return false;
        }
        if (event.which === 37) {
          $('.i13m-widget-button-left').click();
        }
        if (event.which === 39) {
          $('.i13m-widget-button-right').click();
        }
      },
      'home': function (event) {
        if (event.which === 13) {
          return false;
        }
        if (event.which === 37) {
          $('.i13m-widget-button-left').click();
        }
        if (event.which === 39) {
          $('.i13m-widget-button-right').click();
        }
      },
      'they-trust-us': function (event) {
        var direction;
        var newIndex;
        if (event.which === 37) {
          direction = -1;
        } else if (event.which === 39) {
          direction = 1;
        } else {
          return;
        }
        newIndex = Math.max(0, Math.min(sharedVariables.testimonials.length - 1, sharedVariables.testimonialIndex + direction));
        showTestimonials(newIndex);
      },
      'latest-job-offers': function (event) {
        if (event.which === 37) {
          plugins.latestSlider.prevSliderThrottled();
        } else if (event.which === 39) {
          plugins.latestSlider.nextSliderThrottled();
        } else {
          return;
        }
      },
      'latest-updates': function (event) {
        if (!plugins.updateSlider) {
          return;
        }
        if (event.which === 37) {
          plugins.updateSlider.prevSliderThrottled();
        } else if (event.which === 39) {
          plugins.updateSlider.nextSliderThrottled();
        } else {
          return;
        }
      },
      'technologies': function (event) {
        if (event.which === 37) {
          plugins.technologySlider.prevSliderThrottled();
        } else if (event.which === 39) {
          plugins.technologySlider.nextSliderThrottled();
        } else {
          return;
        }
      }
    };

    function addKeyEventWatchers () {
      $(window).on('window:keydown', dispatchKeyEvents);
    }

    function addSliderResizer () {
      $(window).on('resize', function () {
        if (!!plugins.technologySlider.reloadSlider) {
          plugins.technologySlider.reloadSlider();
        }
        if (!!plugins.latestSlider.reloadSlider) {
          plugins.latestSlider.reloadSlider();
        }
        if (!!plugins.portfolioSlider && !!plugins.portfolioSlider.reloadSlider) {
          plugins.portfolioSlider.reloadSlider();
          plugins.portfolioSlider.goToSlide(1);
          plugins.portfolioSlider.goToSlide(0);
        }
      });
    }

    function animateEntry () {
      var newDashOffset = {
        css: {
          strokeDashoffset: 0
        },
        ease: 'linear'
      };
      var wordEntry = {
        css: {
          opacity: 1,
          transform: 'scale(1)',
          textShadow: '0px 0px 0.5px rgb(70, 90, 98)'
        },
        ease: 'ease-in-out'
      };
      var textEntry = {
        css: {
          opacity: 1,
          textShadow: '0px 0px 0.5px rgb(70, 90, 98)'
        },
        ease: 'ease-in-out'
      };
      var timeLine = new TimelineMax({
        onComplete: function () {
          $('#i13m-widget-input').change(function () {
            $(this).focus();
          });
          sharedVariables.carouselObject.movedByClick = true;
          sharedVariables.carouselObject.go(0);
        }
      });
      var timing = {
        stroke: 0.2,
        input: 0.5,
        word: 0.1,
        text: 0.2
      };

      timeLine.delay(-0.6);

      _.each(textElements, function (textElement) {
        timeLine.to(textElement, timing.word, wordEntry);
      });

      timeLine
        .to(upperStroke, timing.stroke, newDashOffset, '-=0.5')
        .to(inputStroke, timing.input, newDashOffset)
        .to(lowerStroke, timing.stroke, newDashOffset, '-=0.2')
        .to(input, timing.word, { opacity: 1 })
        .to(imagine, timing.text, textEntry, '-=0.5')
        .to(carousel, timing.text, textEntry, '-=0.3');
    }

    function animateLink () {
      var timeLine = new TimelineMax();
      TweenMax.killTweensOf(done);
      TweenMax.killTweensOf(link);
      timeLine
        .to(done, 1.5, {
          css: {
            webkitFilter: 'blur(0px)',
            filter: 'blur(0px)',
            opacity: 1,
            textShadow: '0px 0px 0.5px rgb(70, 90, 98)'
          },
          ease: Power3.easeOut
        })
        .to(link, 0.4, {
          css: {
            opacity: 1,
            textShadow: '0px 0px 0.5px rgb(70, 90, 98)',
            cursor: 'pointer'
          }
        });
    }

    function appendBackgrounds () {
      var $container = $('.i13m-home-main-background');
      var background2 = '<img src="//cdn2.hubspot.net/hubfs/2422193/pictures/itmagination-main-background5.jpg" alt="itmagination" style="opacity:0;"/>';
      var background3 = '<img src="//cdn2.hubspot.net/hubfs/2422193/pictures/itmagination-main-background3.jpg" alt="itmagination" style="opacity:0;"/>';
      $container
        .append(background2)
        .append(background3);
    }

    function clearInput () {
      input.val('');
      generateLink();
    }

    function createAnimation (selector) {
      var timeLine = new TimelineMax();
      var mainElement = $(selector).parent().parent();
      var movement = 10;
      timeLine
        .to($(selector).find('.i13m-careers-offer-title'), 0.1, { autoAlpha: 0, display: 'none'})
        .to($(selector).find('.i13m-careers-offer-apply'), 0.1, { autoAlpha: 1, display: 'flex'}, '-=0.1')
        .to([$(selector), $(selector).find('.i13m-careers-offer-apply')], 0.1, { css: { height: '190px' }})
        .to(mainElement, 0.1, { y: '-' + movement, css: { marginTop: '6px', marginBottom: '6px' }},  '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { height: '34px' }}, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-title.additional'), 0.1, { autoAlpha: 1, display: 'block' }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-salary'), 0.1, { autoAlpha: 0, display: 'none' }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-city'), 0.1, { css: { color: '#0098da' } }, '-=0.1')
        .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { backgroundColor: '#2c3e50' } }, '-=0.1');
      return timeLine;
    }

    function dispatchKeyEvents (event) {
      var section = _.last(_.last(window.location.href.split('/')).split('#'));
      var fn = EVENT_FUNCTIONS[section];
      if (!fn) {
        return;
      }
      fn(event);
    }

    function fixPortfolioSlideWith () {
      $('.i13m-home-portfolio-container2')
        .children('li')
        .each(function () {
          $(this).css({width: '100%'});
        });
    }

    function generateLink (text) {
      var value = _.isString(text) ? text : input.val();
      if (value === previousInputValue) {
        return;
      }
      hideLink();
      previousInputValue = value;
      if (!value) {
        return;
      }
      ga('send', 'event', 'Widget', 'type', value);
      makeLink(value);
      setTimeout(function () {
        animateLink();
      }, 400);
    }

    function getVariables () {
      getSharedVariables();
      getScreenVariables();
    }

    function getScreenVariables () {
      sharedVariables.screenHeight = $(window).height();

      sharedVariables.screenWidth = $(window).width();

      if (sharedVariables.screenWidth <= 767) {
        sharedVariables.latestSlides = 1;
        sharedVariables.portfolioSlides = 1;
        sharedVariables.testimonialSlides = 1;
      } else if (sharedVariables.screenWidth <= 1024) {
        sharedVariables.latestSlides = 2;
        sharedVariables.portfolioSlides = 2;
        sharedVariables.testimonialSlides = 3;
      } else {
        sharedVariables.latestSlides = 4;
      }
    }

    function getSharedVariables () {
      sharedVariables.language = window.location.href.split('/')[3];
    }

    function hideLink () {
      var timeLine = new TimelineMax();
      timeLine
        .to(done, 0.4, {
          css: {
            webkitFilter: 'blur(50px)',
            filter: 'blur(50px)',
            textShadow: '0px 0px 20px rgb(70, 90, 98)',
            opacity: 0
          }
        })
        .to(link, 0.4, {
          css: {
            opacity: 0,
            textShadow: '0px 0px 20px rgb(70, 90, 98)',
            cursor: 'none'
          }
        }, '-=0.4');

    }

    function initCarousel () {
      var carouselElement = $('.i13m-widget-carousel');
      carouselElement
        .widgetCarousel({
          buttonLeft: $('.i13m-widget-button-left'),
          buttonRight: $('.i13m-widget-button-right'),
          bringToFront: true,
          onAnimationFinished: sendTextToInput,
          yOrigin: 0,
          yRadius: 0,
          opacityModifier: 0.75,
          farScale: 0.2
        });
      sharedVariables.carouselObject = $('.i13m-widget-carousel').data('i13m-widget-carousel');
    }

    function initPlugins () {
      var fuseOptions = {
        caseSensitive: false,
        matchAllTokens: true,
        tokenize: true,
        threshold: 0.2,
        distance: 1000,
        maxPatternLength: 32,
        id: 'url',
        keys: [{
          name: 'name',
          weight: 0.7
        }, {
          name: 'text',
          weight: 0.3
        }]
      };
      plugins.fuse = new Fuse(WIDGET_QUERY_TEXTS, fuseOptions);
      plugins.latestSlider = $('.bxslider').bxSlider({
        controls: false,
        hideControlOnEnd: true,
        infiniteLoop: true,
        maxSlides: sharedVariables.latestSlides,
        minSlides: sharedVariables.latestSlides,
        moveSlides: sharedVariables.latestSlides,
        pager: false,
        slideWidth: 180,
        slideMargin: 32
      });
      plugins.technologySlider = $('.bxslider-technologies').bxSlider({
        maxSlides: 1,
        minSlides: 1,
        moveSlides: 1,
        controls: false
      });
      setSliderControls(plugins.technologySlider, '.i13m-left-button.technologies', '.i13m-right-button.technologies');

      if (sharedVariables.portfolioSlides) {
        fixPortfolioSlideWith();
        plugins.portfolioSlider = $('.i13m-home-portfolio-container2').bxSlider({
          controls: false,
          maxSlides: sharedVariables.portfolioSlides,
          minSlides: sharedVariables.portfolioSlides,
          moveSlides: sharedVariables.portfolioSlides
        });
        setTimeout(function () {
          $('.i13m-home-portfolio')
          .find('.bx-pager')
          .children('.bx-pager-item')
          .each(function (index) {
            $(this).children().removeClass('active');
            if (index === 0) {
              $(this).children().addClass('active');
            }
          });
        }, 100);
      }

      $('.i13m-updates-grid').i13mPagination({
        controlsSelector: '.i13m-pagination-controls'
      });
      if (sharedVariables.testimonialSlides) {
        plugins.testimonialSlider = $('.i13m-home-testimonials-customers').bxSlider({
          maxSlides: 1,
          minSlides: 1,
          moveSlides: 1,
          controls: false,
          slideWidth: 199.33,
          onSlideAfter: function ($slideElement, oldIndex, newIndex) {
            showTestimonials(newIndex);
          }
        });
      }
      $('.bx-prev, .bx-next').removeAttr('href');
      setSliderControls(plugins.latestSlider, '.i13m-left-button.latest', '.i13m-right-button.latest');

      $('.i13m-map-tooltip.boston').tooltipster({
        animationDuration: 150,
        interactive: true
      });
      $('.i13m-map-tooltip.poland').tooltipster({
        animationDuration: 150,
        interactive: true,
        side: 'right',
        functionBefore: function () {
          TweenMax.to($('.poland.i13m-map-tooltip').children('p'), 0.3, { autoAlpha: 0 });
        },
        functionAfter: function () {
          TweenMax.to($('.poland.i13m-map-tooltip').children('p'), 0.3, { autoAlpha: 1 });
        },
        trigger: 'custom',
        triggerOpen: {
          mouseenter: true
        },
        triggerClose: {
          originClick: true
        }
      });
      $('.i13m-map-tooltip-small').tooltipster({
        animationDuration: 150,
        interactive: true,
        side: 'left'
      });

      $('.i13m-home-portfolio-container2')
        .find('a')
        .filter(function () {
          return $(this).data('modal-open');
        })
        .each(function () {
          var $this = $(this);
          $this.click(function () {
            var className = '.i13m-portfolio-story' + $(this).data('modal-open');
            TweenMax.to($(className), 0.3, { autoAlpha: 1, display: 'block', zIndex: 1 });
          });
        });

      $('.i13m-portfolio-close').click(function () {
        var $this = $(this);
        TweenMax.to($this.parent().parent(), 0.3, { autoAlpha: 0, display: 'none', zIndex: 99 });
      });
    }

    // function loadAdditionalImages () {
    //   var portfolioStyles = [
    //     [
    //       '-webkit-linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-skanska.jpg)',
    //       'linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-skanska.jpg)'
    //     ],
    //     [
    //       '-webkit-linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-danone.jpg)',
    //       'linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-danone.jpg)'
    //     ],
    //     [
    //       '-webkit-linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-bayer.jpg)',
    //       'linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-bayer.jpg)'
    //     ],
    //     [
    //       '-webkit-linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-nfg.jpg)',
    //       'linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-nfg.jpg)'
    //     ],
    //     [
    //       '-webkit-linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-add.jpg)',
    //       'linear-gradient(rgba(0, 85, 122, .8), rgba(0, 85, 122, .8)),url(//cdn2.hubspot.net/hubfs/2422193/pictures/portfolio/portfolio-add.jpg)'
    //     ]
    //   ];
    //   var $portfolioContainer = $('.i13m-home-portfolio-container2');
    //   var $modals;
    //   if (sharedVariables.screenWidth < 1024) {
    //     $modals = $portfolioContainer
    //       .children()
    //       .children();
    //   } else {
    //     $modals = $portfolioContainer
    //       .children();
    //   }
    //   $modals
    //     .each(function (index) {
    //       var $child = $(this);
    //       _.each(portfolioStyles[index], function (style) {
    //         console.log(index, style, $child);
    //         $child.css({ 'background-image': style });
    //       });
    //     });
    //
    // }

    function setSliderControls (slider, leftSelector, rightSelector) {
      slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
      slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
      $(leftSelector).on('click', function () {
        slider.prevSliderThrottled();
        return false;
      });
      $(rightSelector).on('click', function () {
        slider.nextSliderThrottled();
        return false;
      });
    }

    function makeLink (text) {
      if (!text) {
        return;
      }
      var urlArray = window.location.href.split('/');
      var output = {};
      var address = _.first(plugins.fuse.search(text));
      address = _.isUndefined(address) ? 'contact' : 'services/' + address;
      output.url = '/' + urlArray.slice(3, 4) + '/' + address;
      updateLinkButton(output);
    }

    function onResize () {
      setTimeout(function () {
        scaleWidget();
      }, 100);
    }

    function parseTestimonials () {
      sharedVariables.testimonials = [];
      $('.i13m-home-testimonials-data')
        .find('tr')
        .each(function (rowIndex) {
          var $tr = $(this);
          $tr
            .children()
            .each(function (cellIndex) {
              var $cell = $(this);
              var value = $cell.html();
              if (!rowIndex) {
                sharedVariables.testimonials[cellIndex] = {
                  company: value
                };
              } else if (rowIndex === 1) {
                sharedVariables.testimonials[cellIndex].person = value;
              } else if (rowIndex === 2) {
                sharedVariables.testimonials[cellIndex].post = value;
              } else if (rowIndex === 3) {
                sharedVariables.testimonials[cellIndex].text = value;
              } else if (rowIndex === 4) {
                sharedVariables.testimonials[cellIndex].image = value;
              }
            });
        });
    }

    function parseTooltips () {
      $('#i13m-map-tooltips')
        .children()
        .each(function () {
          if (!this.dataset.xCoordinate || !this.dataset.yCoordinate) {
            return;
          }
          tooltips.push({
            x: this.dataset.xCoordinate,
            y: this.dataset.yCoordinate,
            el: $(this)
          });
        });
    }

    function renderTestimonials () {
      if (true) {
        var template =
          '<div class="i13m-home-testimonial">' +
            '<div class="i13m-home-testimonial-text"><p></p></div>' +
            '<div class="i13m-home-testimonial-person"><p></p></div>' +
            '<div class="i13m-home-testimonial-post"><p></p></div>' +
            '<div class="i13m-home-testimonial-company"><p></p></div>' +
          '</div>';
        $('.i13m-home-testimonials-container').append(template);
      } else {
        _.each(sharedVariables.testimonials, function (testimonial) {
          var template =
            '<div>' +
              '<a class="i13m-home-testimonial-' + testimonial.image + '"></a>' +
              '<div class="i13m-home-testimonial">' +
                '<div class="i13m-home-testimonial-text"><p></p></div>' +
                '<div class="i13m-home-testimonial-person"><p></p></div>' +
                '<div class="i13m-home-testimonial-post"><p></p></div>' +
                '<div class="i13m-home-testimonial-company"><p></p></div>' +
              '</div>' +
            '</div>';
          $('.i13m-home-testimonials-mobile').append(template);
        });
      }
    }

    function scaleWidget () {
      var scale = $(window).width() / 1920;
      $('.i13m-widget-table')
        .css(transform, 'scale(' + scale + ')');
    }

    function sendTextToInput () {
      var value = sharedVariables.carouselObject.nearestItem().element.innerHTML.toLowerCase();
      var text = WIDGET_TEXTS[value];
      if (!sharedVariables.carouselObject.movedByClick) {
        return;
      }
      turnInputWatcherOff();
      clearInput();
      return appendTextToInput(text)
        .then(function () {
          generateLink(value);
          return this;
        })
        .then(turnInputWatcherOn);
    }

    function turnInputWatcherOff () {
      $('#i13m-widget-input')
        .unbind('keyup keypress change');
    }

    function turnInputWatcherOn () {
      $('#i13m-widget-input')
        .on('keyup keypress change', _.debounce(generateLink, 400));
    }

    function appendTextToInput (text) {
      var deferred = $.Deferred();
      var length = text.length;
      for (var i = 0; i < length; i++) {
        var last = i === (length - 1);
        (function (index, isLast) {
          setTimeout(function () {
            input
              .val(text.substr(0, index + 1));
            if (isLast) {
              deferred.resolve();
            }
          }, index * (700 / length));
        }(i, last));
      }

      return deferred;
    }

    function setHoverWatchers () {
      $('.i13m-careers-offer-link')
        .each(function () {
            $(this).hover(function () {
              if (!this.animation) {
                this.animation = createAnimation(this);
              } else {
                this.animation.play().timeScale(1);
              }
          }, function () {
            if (!this.animation) {
              this.animation = createAnimation(this);
            } else {
              this.animation.reverse().timeScale(4);
            }
          });
        });
    }

    function setClickWatchers () {
      var url = window.location.href.split('/');
      $('#i13m-widget-input')
        .on('keyup keypress change', _.debounce(generateLink, 400))
        .on('click', clearInput);
      $('.i13m-careers-latest-all').attr('href', _.first(url) + '//' + CAREERS_URL + '/'  + sharedVariables.language + '#job-offers');
    }

    function setTestimonialWatchers () {
      $('.i13m-home-testimonials-customers')
        .children()
        .each(function (index) {
          var $this = $(this);
          $this.hover(function () {
            showTestimonials(index);
          }, _.noop);
        });
    }

    function setWatchers () {
      setClickWatchers();
      setHoverWatchers();
      setTestimonialWatchers();
      setupContactWatchers();
    }

    function setupBackgroundAnimation (newIndex) {
      var imagesNumber = 3;
      var normalizedNewIndex = (newIndex + 1) % imagesNumber;
      var timeDelay = 5;
      var $images = $('.i13m-home-main-background').children('img');
      var timeLine = new TimelineMax({
        onComplete: function () {
          setupBackgroundAnimation(normalizedNewIndex);
        }
      });
      timeLine
        .delay(timeDelay)
        .to($images, 1, { autoAlpha: 0 })
        .to($images.filter(function (index) {
          return normalizedNewIndex === index % imagesNumber;
        }), 1, { autoAlpha: 0.5 }, '-=1');
    }

    function setupContactWatchers () {

      var setSuccess = setInterval(function () {
        if (!$('.i13m-home-contact-form').find('.hs-form').length) {
          return;
        } else {
          clearInterval(setSuccess);
          $('.i13m-home-contact-form')
            .find('form')
            .on('submit', function(e) {
              var areAllFieldsOK = true;
              $(this)
                .find('input[required]')
                .each(function () {
                  if ($(this).attr('class').indexOf('error') > -1) {
                    areAllFieldsOK = false;
                  }
                });
                if (areAllFieldsOK) {
                } else {
                  e.preventDefault();
                  return false;
                }
            });
        }
      }, 100);
    }

    function showSliderOffers () {
      $('.bxslider')
        .find('.i13m-careers-offer')
        .css({ display: 'block'});
    }

    function showTestimonials (showIndex) {
      var timeLine = new TimelineMax();
      var $testimonial = $('.i13m-home-testimonial');
      var bxOffset = sharedVariables.testimonialSlides ? 1 : 0;
      sharedVariables.testimonialIndex = showIndex;
      $('.i13m-home-testimonials-customers')
        .children()
        .removeClass('selected')
        .filter(function (index) {
          return index === showIndex + bxOffset;
        })
        .addClass('selected');
      timeLine
        .to($testimonial.find('p'), 0.3, { autoAlpha: 0, onComplete: function () {
          _.each(['text', 'person', 'post', 'company'], function (category) {
            $testimonial
              .find('.i13m-home-testimonial-' + category)
              .children('p')
              .html(sharedVariables.testimonials[showIndex][category]);
          });
        }}, 'one')
        .to($testimonial.find('p'), 0.3, { autoAlpha: 1 }, 'two');
    }

    function sortAndFilterUpdates () {
      $('.i13m-updates-grid')
        .children('.grid-item')
        .filter(function (index) {
          return index > 5;
        })
        .each(function () {
          $(this).css({ display: 'none' });
        });
    }

    function updateCoordinates (tooltip, screenWidth, screenHeight, leftMargin, topMargin, imageScaleRatio) {
      var left = (tooltip.x * imageScaleRatio - (TOOLTIP_SIZE / 2) * screenHeight + leftMargin) / screenWidth;
      var top = (tooltip.y * imageScaleRatio - (TOOLTIP_SIZE / 2) * screenHeight + topMargin) / screenHeight;
      tooltip.el.css({
        left: left * 100 + '%',
        top: top * 100 + '%'
      });
    }

    function updateLinkButton (output) {
      link.attr('href', output.url);
    }

    function updateMap () {
      var html = document.documentElement;

      var screenHeight = Math.max(
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
      );

      var screenWidth = Math.max(
        html.clientWidth,
        html.scrollWidth,
        html.offsetWidth
      );
      var imageContainerWidth = screenWidth - 2 * padding * screenHeight;
      var imageContainerHeight = screenHeight * 0.5 * (1 - 2 * padding) - 72;
      var containerAspectRatio = imageContainerWidth / imageContainerHeight;
      var actualImageWidth;
      var actualImageHeight;
      var leftMargin;
      var topMargin;
      var imageScaleRatio;
      if (containerAspectRatio > IMAGE_ASPECT_RATIO) {
        actualImageHeight = imageContainerHeight;
        actualImageWidth = IMAGE_ASPECT_RATIO * actualImageHeight;
      } else {
        actualImageWidth = imageContainerWidth;
        actualImageHeight = actualImageWidth / IMAGE_ASPECT_RATIO;
      }
      imageScaleRatio = actualImageWidth / IMAGE_WIDTH;
      leftMargin = (imageContainerWidth - actualImageWidth) / 2;
      topMargin = (imageContainerHeight - actualImageHeight) / 2;

      _.each(tooltips, function (tooltip) {
        updateCoordinates(tooltip, imageContainerWidth, imageContainerHeight, leftMargin, topMargin, imageScaleRatio);
      });

    }

    function onReady () {
      getVariables();
      clearInput();
      onResize();
      $(window).on('window:resize', function () {
        onResize();
      });
      $('#i13m-widget-chosen-section').after(' ');
      initCarousel();
      addKeyEventWatchers();
      initPlugins();
      showSliderOffers();
      setWatchers();
      parseTooltips();
      updateMap();
      $(window).on('window:resize', function () {
        updateMap();
      });
      parseTestimonials();
      renderTestimonials();
      showTestimonials(0);
      sortAndFilterUpdates();
      addSliderResizer();
      appendBackgrounds();
      setupBackgroundAnimation(1);
      // loadAdditionalImages();
    }

    $(document).ready(onReady);

    $(window).load(function () {
      animateEntry();
    });

  }());
}