export default function i13mPagination($) {
  'use strict';

  $.i13mPagination = function (element, options) {
    var pluginName = 'i13mPagination';
    var itemCount;
    var defaults = {
      itemSelector: '.grid-item',
      startPage: 1
    };

    var plugin = this;

    function addControls () {
      var timeLine = new TimelineMax();
      var prevButtonTemplate = '<a class="i13m-left-button pagination"></a>';
      var nextButtonTemplate = '<a class="i13m-right-button pagination"></a>';
      var $controlsElement = $(plugin.settings.controlsSelector);

      timeLine.to($controlsElement, 0, { autoAlpha: 0 });

      $controlsElement
        .children()
        .remove();

      $controlsElement
        .append(prevButtonTemplate);

      $controlsElement
        .append(nextButtonTemplate);

      timeLine.to($controlsElement, 0.3, { autoAlpha: 1 });

      if (plugin.settings.pageItems === 6) {
        return;
      }
      $controlsElement
        .children('.i13m-left-button.pagination')
        .each(function () {
          $(this).on('click', function () {
            applyFiltersAndGoToPageThrottled(plugin.pageNumber - 1);
          });
        });

      $controlsElement
        .children('.i13m-right-button.pagination')
        .each(function () {
          $(this).on('click', function () {
            applyFiltersAndGoToPageThrottled(plugin.pageNumber + 1);
          });
        });
    }

    var applyFiltersAndGoToPageThrottled = _.throttle(applyFiltersAndGoToPage, 300, { leading: true, trailing: false });

    function applyFiltersAndGoToPage (pageNumber, shouldRefresh) {
      var pageCount;
      if (
        !shouldRefresh &&
        (pageNumber === plugin.pageNumber || pageNumber < 1 || pageNumber > plugin.pageCount)
      ) {
        return;
      }
      var firstIndex = (pageNumber - 1) * plugin.settings.pageItems;
      var lastIndex = pageNumber * plugin.settings.pageItems - 1;
      var offersOut = $(plugin.element)
        .children(plugin.settings.itemSelector);
      var offersIn = $(plugin.element)
        .children(plugin.settings.itemSelector);

      pageCount = Math.ceil(offersIn.length / plugin.settings.pageItems);

      if (pageNumber > pageCount || pageNumber < 1) {
        return;
      }
      offersIn = offersIn
        .filter(function (index) {
          return index >= firstIndex && index <= lastIndex;
        });
      var timeLine = new TimelineMax();

      timeLine.to(offersOut, 0.3, { autoAlpha: 0, display: 'none' });
      timeLine.to(offersIn, 0.3, { autoAlpha: 1, display: 'block' });

      plugin.pageNumber = pageNumber;
      processControlButtons (pageCount);
    }

    function processControlButtons (pageCount) {
      var $controlsElement = $(plugin.settings.controlsSelector);
      var prevButton = $controlsElement.children('.i13m-left-button.pagination');
      var nextButton = $controlsElement.children('.i13m-right-button.pagination');
      if (plugin.pageNumber === 1) {
        prevButton.addClass('disabled');
      } else {
        prevButton.removeClass('disabled');
      }
      if (plugin.pageNumber === pageCount) {
        nextButton.addClass('disabled');
      } else {
        nextButton.removeClass('disabled');
      }
    }

    function setDefaults () {
      plugin._defaults.pageItems = itemCount;
    }

    function setItemCount () {
      var screenHeight = $(window).height();
      var screenWidth = $(window).width();

      if (screenWidth <= 767) {
        itemCount = 2;
      } else if (screenWidth <= 1024) {
        itemCount = 4;
      } else {
        itemCount = 6;
      }

      if (screenHeight <= 890) {
        itemCount /= 2;
      }
    }

    function setPageCount () {
      plugin.pageCount = Math.ceil($(plugin.element).children(plugin.settings.itemSelector).length / plugin.settings.pageItems);
    }

    function init () {
      plugin.element = element;

      plugin._filters = {};
      plugin._defaults = defaults;
      plugin._name = pluginName;
      plugin._offers = [];
      setItemCount();
      setDefaults();
      plugin.settings = $.extend({}, defaults, options);
      setPageCount();
      addControls();
      applyFiltersAndGoToPage(plugin.settings.startPage);
    }

    $.extend($.i13mPagination.prototype, {
      init: init
    });

    plugin.init();
  };

  $.fn.i13mPagination = function (options) {
    return this.each(function () {
        if (undefined === $(this).data('i13mPagination')) {
          var plugin = new $.i13mPagination(this, options);
          $(this).data('pluginName', plugin);
        }
    });
  };

};
