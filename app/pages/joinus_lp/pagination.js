'use strict';
import Fuse from 'fuse.js';

export default function i13mPagination($) {
  $.i13mPagination = function (element, options) {
    var pluginName = 'i13mPagination';
    var itemCount;
    var defaults = {
      itemSelector: '.i13m-careers-offer',
      startPage: 1
    };
    var manualScrollDuration = 400;
    var automaticScrollDuration = 1500;
    var offersPageIndex = 10;
    var searchInputs = {
      search: $('.i13m-careers-offers-text'),
      city: $('.i13m-careers-offers-city'),
      specialization: $('.i13m-careers-offers-specialization')
    };

    var sharedVariables = {
        specializations: {
            en: [{
                value: "software",
                label: "Software Development"
            }, {
                value: "data_management",
                label: "Business Intelligence & Data Management"
            }, {
                value: "data_science",
                label: "Data Science"
            }, {
                value: "management",
                label: "Management & Consulting"
            }, {
                value: "analysis",
                label: "Analysis"
            }, {
                value: "quality",
                label: "Quality Assurance"
            }, {
                value: "sales",
                label: "Sales & Business Development"
            }, {
                value: "it_supp_infra",
                label: "IT Support & Infrastructure"
            }, {
                value: "marketing",
                label: "PR & Marketing"
            }, {
                value: "hr_recruitment",
                label: "HR & Recruitment"
            }],
            pl: [{
                value: "software",
                label: "Software Development"
            }, {
                value: "data_management",
                label: "Business Intelligence & Data Management"
            }, {
                value: "data_science",
                label: "Data Science"
            }, {
                value: "management",
                label: "Management & Consulting"
            }, {
                value: "analysis",
                label: "Analysis"
            }, {
                value: "quality",
                label: "Quality Assurance"
            }, {
                value: "sales",
                label: "Sales & Business Development"
            }, {
                value: "it_supp_infra",
                label: "IT Support & Infrastructure"
            }, {
                value: "marketing",
                label: "PR & Marketing"
            }, {
                value: "hr_recruitment",
                label: "HR & Recruitment"
            }],
            de: [{
                value: "software",
                label: "Software Development"
            }, {
                value: "data_management",
                label: "Business Intelligence & Data Management"
            }, {
                value: "data_science",
                label: "Data Science"
            }, {
                value: "management",
                label: "Management & Consulting"
            }, {
                value: "analysis",
                label: "Analysis"
            }, {
                value: "quality",
                label: "Quality Assurance"
            }, {
                value: "sales",
                label: "Sales & Business Development"
            }, {
                value: "it_supp_infra",
                label: "IT Support & Infrastructure"
            }, {
                value: "marketing",
                label: "PR & Marketing"
            }, {
                value: "hr_recruitment",
                label: "HR & Recruitment"
            }]
        },
      language: _.first(window.location.href.split('/')[3].split('#'))
    };

    var plugin = this;

    function addControls () {
      var timeLine = new TimelineMax();
      var prevButtonTemplate = '<a class="i13m-left-button pagination"></a>';
      var nextButtonTemplate = '<a class="i13m-right-button pagination"></a>';
      var $controlsElement = $(plugin.settings.controlsSelector);

      timeLine.to($controlsElement, 0, { autoAlpha: 0 });

      $controlsElement
        .children()
        .remove();

      $controlsElement
        .append(prevButtonTemplate);

      $controlsElement
        .append(nextButtonTemplate);

      timeLine.to($controlsElement, 0.3, { autoAlpha: 1 });

      $controlsElement
        .children('.i13m-left-button.pagination')
        .each(function () {
          $(this).on('click', function () {
            moveLeft();
          });
        });

      $controlsElement
        .children('.i13m-right-button.pagination')
        .each(function () {
          $(this).on('click', function () {
            moveRight();
          });
        });
    }

    var applyFiltersAndGoToPageThrottled = _.debounce(applyFiltersAndGoToPage, 500, {leading: false, trailing: true});

    function applyFiltersAndGoToPage (pageNumber, shouldRefresh) {
      setTimeout(function () {
        var pageCount;
        var firstIndex;
        var lastIndex;
        var offersOut;
        var offersIn;        
        
        $('.no_results').hide();
        $('.i13m-careers-offers-list').show();
        $('.i13m-careers-offers-controls').css('opacity',1);
        
        if (
          !shouldRefresh &&
          (pageNumber === plugin.pageNumber || pageNumber < 1 || pageNumber > plugin.pageCount)
        ) {
          return;
        }
        firstIndex = (pageNumber - 1) * plugin.settings.pageItems;
        lastIndex = pageNumber * plugin.settings.pageItems - 1;
        offersOut = $(plugin.element)
        .children(plugin.settings.itemSelector);
        offersIn = $(plugin.element)
        .children(plugin.settings.itemSelector)
        .filter(function () {
          var normalizedSpecialization = plugin._filters.specialization && _.first(_.filter(sharedVariables.specializations[sharedVariables.language], function (specialization) {
            return specialization.label.toLowerCase() === plugin._filters.specialization.toLowerCase();
          }));
          var isCityOK = plugin._filters && plugin._filters.city
          ? plugin._filters.city.toLowerCase() === $(this).find('.i13m-careers-offer-city').html().trim().toLowerCase()
          : true;
          var isSpecializationOK = plugin._filters && plugin._filters.specialization
          ? normalizedSpecialization.value === $(this).find('.i13m-careers-offer-specialization').html().trim().toLowerCase()
          : true;
          if (!(isCityOK && isSpecializationOK)) {            
            return false;
          }
          if (!(plugin._filters && plugin._filters.search)) {            
            return true;
          }
          var correctIdsArray = getProp(plugin._fuse.search(plugin._filters.search), 'id');
         

          return _.indexOf(correctIdsArray, $(this).find('.i13m-careers-offer-id').html().trim()) > -1;
        });
        
        if(offersIn.length === 0) {          
          
          $('.i13m-careers-offers-list').hide();
          $('.no_results').show();
          $('.i13m-careers-offers-controls').css('opacity',0);                 
        }

        pageCount = Math.ceil(offersIn.length / plugin.settings.pageItems);

        if (pageNumber > pageCount || pageNumber < 1) {
          return;
        }         
       
        offersIn = offersIn
        .filter(function (index) {
          return index >= firstIndex && index <= lastIndex;
        });
        var timeLine = new TimelineMax();

        timeLine.to(offersOut, 0.3, { autoAlpha: 0, display: 'none' });
        timeLine.to(offersIn, 0.3, { autoAlpha: 1, display: 'block' });

        plugin.pageNumber = pageNumber;
        processControlButtons (pageCount);
      }, 0)
    }

    function constructOffers () {
      $('.i13m-careers-offers-list')
        .find('.i13m-careers-offer')
        .each(function () {
          var offer = {};
          $(this)
            .find('.i13m-careers-offer-details')
              .children()
              .each(function () {
                var field = _.last(_.first($(this).attr('class').split(' ')).split('-'));
                offer[field] = $(this).text().trim().replace(/(\s\s+|\n)/g, ' ');
              });
          plugin._offers.push(offer);
        });
    }

    function getProp (array, prop) {
      return _.map(array, function (member) {
        return member[prop];
      });
    }

    function handleButtonClick (autoCompleteElement) {
      if (autoCompleteElement.ul.childNodes.length === 0) {
        autoCompleteElement.minChars = 0;
        autoCompleteElement.evaluate();
      } else if (autoCompleteElement.ul.hasAttribute('hidden')) {
        autoCompleteElement.open();
      } else {
        autoCompleteElement.close();
      }
    }

    function initAutocompletes () {

      plugin.specializations =  _.map(sharedVariables.specializations[sharedVariables.language], function (specialization) { return specialization.label; });

      var cityInput = new Awesomplete(document.getElementById('i13m-careers-offers-city'), {
        list: plugin.cities,
        minChars: 0
      });

      Awesomplete.$('.i13m-careers-offers-city-button').addEventListener('click', function () {
        handleButtonClick(cityInput);
      });

      var specializationInput = new Awesomplete(document.getElementById('i13m-careers-offers-specialization'), {
        list: plugin.specializations,
        minChars: 0
      });

      Awesomplete.$('.i13m-careers-offers-specialization-button').addEventListener('click', function () {
        handleButtonClick(specializationInput);
      });
    }

    function moveLeft () {
      applyFiltersAndGoToPageThrottled(plugin.pageNumber - 1);
    }

    function moveRight () {
      applyFiltersAndGoToPageThrottled(plugin.pageNumber + 1);
    }

    function passSearchToFilters () {
      var searchInput = $('.i13m-careers-main-search-input');
      var searchTerms = searchInput.val().trim().split(' ');
      var filterObject = {};
      if (searchTerms.length === 0) {
        return;
      }
      _.each(searchTerms, function (term) {
        term = term.toLowerCase();
        var casedTerm = _.capitalize(term);
        var cities = getProp(plugin._cityFuse.search(casedTerm), 'city');
        if (!!cities.length) {
          filterObject.city = _.first(cities);
          filterObject.search = filterObject.search ? filterObject.search + ' ' + term : term;
        } else {
          filterObject.search = filterObject.search ? filterObject.search + ' ' + term : term;
        }
      });
      _.forOwn(searchInputs, function (value, key) {
        var filterValue = filterObject[key];
        if (!!filterValue) {
          plugin._filters[key] = filterValue;
        } else {
          delete plugin._filters[key];
        }
        searchInputs[key].val(filterValue);
      });
      $.fn.fullpage.setScrollingSpeed(automaticScrollDuration);
      $.fn.fullpage.moveTo(offersPageIndex);
      $.fn.fullpage.setScrollingSpeed(manualScrollDuration);
      searchInput.val('');
      sendSearchTerms();
      applyFiltersAndGoToPageThrottled(1, true);
    }

    function populateCities () {
      var cities = [];
      $(plugin.settings.itemSelector)
        .each(function () {
          var city = $(this)
            .find('.i13m-careers-offer-city')
            .html();
          cities = _.union(cities, [city.trim()]);
        });
      plugin.cities = _.sortBy(cities);
    }

    function processControlButtons (pageCount) {
      var $controlsElement = $(plugin.settings.controlsSelector);
      var prevButton = $controlsElement.children('.i13m-left-button.pagination');
      var nextButton = $controlsElement.children('.i13m-right-button.pagination');
      if (plugin.pageNumber === 1) {
        prevButton.addClass('disabled');
      } else {
        prevButton.removeClass('disabled');
      }
      if (plugin.pageNumber === pageCount) {
        nextButton.addClass('disabled');
      } else {
        nextButton.removeClass('disabled');
      }
    }

    function processFilters (event) {
      var previousFilters = _.merge({}, plugin._filters);
      var $element = $(event.target);
      var inputValue = $element.val();
      if ($element.attr('class') === 'i13m-careers-offers-city') {
        if (_.indexOf(plugin.cities, inputValue) < 0) {
          delete plugin._filters.city;
        } else {
          plugin._filters.city = inputValue;
        }
      } else if ($element.attr('class') === 'i13m-careers-offers-specialization') {
        if (_.indexOf(plugin.specializations, inputValue) < 0) {
          delete plugin._filters.specialization;
        } else {
          plugin._filters.specialization = inputValue;
        }
      } else {
        if (!!inputValue) {
          plugin._filters.search = inputValue;
        } else {
          delete plugin._filters.search;
        }
      }
      if (!_.isEqual(previousFilters, plugin._filters)) {
        sendSearchTerms();
        applyFiltersAndGoToPage(1, true);
      }
    }

    function sendSearchTerms () {
      if (plugin._filters.search) {
        ga('send', 'event', 'Careers', 'search', plugin._filters.search);
      }
      if (plugin._filters.city) {
        ga('send', 'event', 'Careers', 'city', plugin._filters.city);
      }
      if (plugin._filters.specialization) {
        ga('send', 'event', 'Careers', 'specialization', plugin._filters.specialization);
      }
    }

    function setDefaults () {
      plugin._defaults.pageItems = itemCount;
    }

    function setItemCount () {
      var screenHeight = $(window).height();
      var screenWidth = $(window).width();

      if (screenWidth <= 767) {
        itemCount = 1;
      } else if (screenWidth <= 1024) {
        itemCount = 2;
      } else {
        itemCount = 4;
      }
      //
      // if (screenHeight <= 890) {
      //   itemCount /= 2;
      // }
    }

    function setPageCount () {
      plugin.pageCount = Math.ceil($(plugin.element).children(plugin.settings.itemSelector).length / plugin.settings.pageItems);
    }

    function setupFilterWatchers () {
      $('.i13m-careers-offers-search')
        .find('input')
        .on('input awesomplete-selectcomplete', _.debounce(processFilters, 300, {
          leading: false,
          trailing: true
        }));
    }

    function setupFuse () {
      var options = {
        caseSensitive: false,
        matchAllTokens: true,
        threshold: 0.2,
        distance: 1000,
        maxPatternLength: 20,
        keys: [
          {
            name: 'title',
            weight: 0.9
          }, {
            name: 'city',
            weight: 0.3
          }, {
            name: 'specialization',
            weight: 0.3
          }, {
            name: 'reference_number',
            weight: 0.3
          }, {
            name: 'responsibilities',
            weight: 0.3
          }, {
            name: 'requirements',
            weight: 0.3
          }, {
            name: 'offer',
            weight: 0.3
          }, {
            name: 'salary',
            weight: 0.3
          }

          // 'title',
          // 'city',
          // 'salary',
          // 'specialization',
          // 'reference_number',
          // 'intro',
          // 'responsibilities',
          // 'requirements',
          // 'offer'
        ]
      };
      var cityOptions = {
        keys: ['city']
      };
      var specializationOptions = {
        keys: ['specialization']
      };
      var cities = _.map(plugin.cities, function (city) {
        return {city: city};
      });
      var specializations = _.map(plugin.specializations, function (specialization) {
        return {specialization: specialization};
      });
      plugin._fuse = new Fuse(plugin._offers, options);
      plugin._cityFuse = new Fuse(cities, cityOptions);
      plugin._specializationFuse = new Fuse(specializations, specializationOptions);
    }

    function setupSearchWatchers () {
      $('.i13m-careers-main-search-button')
        .on('click', function () {
          passSearchToFilters();
        });

      $('.i13m-careers-main-search-input')
        .on('keydown', function (event) {
          if (event.which === 13) {
            passSearchToFilters();
          }
        });

      $('.i13m-careers-latest-all')
        .on('click', function () {
          plugin._filters = {};
          applyFiltersAndGoToPage(1, true);
          _.each(searchInputs, function (searchInput) {
            $(searchInput).val('');
          });
          slowMoveTo(offersPageIndex);
        });
      $('.i13m-careers-logo')
        .on('click', function () {
          slowMoveTo('main');
        });

        $('.i13m-careers-main-popular-searches')
        .on('click', function () {
          var getPopularKeyword = $(this).text();
          var searchInput = $('.i13m-careers-main-search-input');
          searchInput.val(getPopularKeyword);
          passSearchToFilters();
          slowMoveTo(offersPageIndex);
        });
    }

    function slowMoveTo (index) {
      $.fn.fullpage.setScrollingSpeed(automaticScrollDuration);
      $.fn.fullpage.moveTo(index);
      $.fn.fullpage.setScrollingSpeed(manualScrollDuration);
    }

    function init () {
      plugin.element = element;

      plugin._filters = {};
      plugin._defaults = defaults;
      plugin._name = pluginName;
      plugin._offers = [];
      setItemCount();
      setDefaults();
      plugin.settings = $.extend({}, defaults, options);
      setPageCount();
      addControls();
      populateCities();
      initAutocompletes();
      setupFilterWatchers();
      setupSearchWatchers();
      constructOffers();
      setupFuse();
      applyFiltersAndGoToPage(plugin.settings.startPage);
    }

    $.extend($.i13mPagination.prototype, {
      init: init,
      moveLeft: moveLeft,
      moveRight: moveRight
    });

    plugin.init();
  };

  $.fn.i13mPagination = function (options) {
    return this.each(function () {
      if (undefined === $(this).data('i13mPagination')) {
        var plugin = new $.i13mPagination(this, options);
        $(this).data('i13mPagination', plugin);
      }
    });
  };

};
