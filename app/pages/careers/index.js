'use strict';

import Fuse from 'fuse.js';
import TweenMax from 'gsap';
import _ from 'lodash';
import 'store';
import '../../../node_modules/bxslider/dist/jquery.bxslider.min.js';
import 'fullpage.js';
import '../shared/index.js';
import '../shared/hotjar.js';
import '../shared/ga.js';
import 'awesomplete';
import 'jquery';
import 'jquery-modal';

import i13mPagination from './pagination';


export default function careers(){
    
    i13mPagination($);

    (function ($) {
        'use strict';
        var startingSection = window.location.hash.split('#')[1];

        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':'\u0104'},
            {'base':'C', 'letters':'\u0106'},
            {'base':'E', 'letters':'\u0118'},
            {'base':'L', 'letters':'\u0141'},
            {'base':'N', 'letters':'\u0143'},
            {'base':'O', 'letters':'\u00D3'},
            {'base':'S', 'letters':'\u015A'},
            {'base':'Z', 'letters':'\u0179\u017B'},
            {'base':'a', 'letters':'\u0105'},
            {'base':'c', 'letters':'\u0107'},
            {'base':'e', 'letters':'\u0119'},
            {'base':'l', 'letters':'\u0142'},
            {'base':'n', 'letters':'\u0144'},
            {'base':'o', 'letters':'\u00F3'},
            {'base':'s','letters':'\u015B'},
            {'base':'z','letters':'\u017A\u017C'}
        ];

        var diacriticsMap = {};
        for (var i=0; i < defaultDiacriticsRemovalMap.length; i++){
            var letters = defaultDiacriticsRemovalMap[i].letters;
            for (var j=0; j < letters.length ; j++){
                diacriticsMap[letters[j]] = defaultDiacriticsRemovalMap[i].base;
            }
        }

        var EVENT_FUNCTIONS = {
            'main': function (event) {
                if (event.which === 37) {
                    plugins.latestSlider.prevSliderThrottled();
                } else if (event.which === 39) {
                    plugins.latestSlider.nextSliderThrottled();
                } else {
                    return;
                }
            },
            'careers': function (event) {
                if (event.which === 37) {
                    plugins.latestSlider.prevSliderThrottled();
                } else if (event.which === 39) {
                    plugins.latestSlider.nextSliderThrottled();
                } else {
                    return;
                }
            },
            'latest-jobs': function (event) {
                if (event.which === 37) {
                    plugins.latestSlider.prevSliderThrottled();
                } else if (event.which === 39) {
                    plugins.latestSlider.nextSliderThrottled();
                } else {
                    return;
                }
            },
            'job-offers': function (event) {
                if (event.which === 37) {
                    $('.i13m-careers-offers-list').data('i13mPagination').moveLeft();
                } else if (event.which === 39) {
                    $('.i13m-careers-offers-list').data('i13mPagination').moveRight();
                } else {
                    return;
                }
            }
        };

        var sharedVariables = {
            successStories: {
                person1: {},
                person2: {},
                person3: {},
                person4: {}
            },
            successStoriesIndex: 'person1',
            successStoriesStoryIndex: 1,
            successStoriesDict: {}
        };

        var plugins = {};
        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

        function addKeyEventWatchers () {
            $(window).on('window:keydown', dispatchKeyEvents);
        }

        function addSliderResizer () {
            $(window).on('resize', function () {
                if (!!plugins.latestSlider && !!plugins.latestSlider.reloadSlider) {
                    plugins.latestSlider.reloadSlider();
                }
            });
        }

        function initPlugins () {
            if($('.bxslider').length) {
                plugins.latestSlider = $('.bxslider').bxSlider({
                   // auto: true,                    
                    controls: false,
                    hideControlOnEnd: true,
                    infiniteLoop: true,
                    maxSlides: sharedVariables.latestSlides,
                    minSlides: sharedVariables.latestSlides,
                    moveSlides: sharedVariables.latestSlides,
                    pager: false,
                    slideWidth: 180,
                    slideMargin: 32
                });
                setSliderControls(plugins.latestSlider, '.i13m-left-button.latest', '.i13m-right-button.latest');
            }
            plugins.pagination = $('.i13m-careers-offers-list').i13mPagination({
                controlsSelector: '.i13m-careers-offers-controls'
            });
            plugins.paginationB = $('.bxslider-test').i13mPagination({
                controlsSelector: '.i13m-careers-offers-controls'
            });

            if (sharedVariables.recruitmentSlides && $('.bxslider-recruitment-mobile').length) {

                plugins.recruitmentSlider = $('.bxslider-recruitment-mobile').bxSlider({
                    controls: false,
                    maxSlides: sharedVariables.recruitmentSlides,
                    minSlides: sharedVariables.recruitmentSlides,
                    moveSlides: 1,
                    slideWidth: 120,
                    slideMargin: 10,
                    infiniteLoop: false
                });
                setSliderControls(plugins.recruitmentSlider, '.i13m-left-button.recruitment', '.i13m-right-button.recruitment');
            }

            $.modal.defaults.closeClass = 'i13m-modal-close';
            $.modal.defaults.fadeDuration = 100;
        }

        function closeStoryControl () {
            var timeLine = new TimelineMax();
            timeLine
                .to($('.i13m-careers-itmw-success-content-front'), 0.6, {
                    css: {
                        rotationY: 0,
                        zIndex: 2,
                        backfaceVisibility: 'hidden'
                    }
                })
                .to($('.i13m-success-control'), 0.6, {
                    css: {
                        rotationY: 180,
                        zIndex: 1,
                        backfaceVisibility: 'hidden'
                    }
                }, '-=0.6');
        }

        function createAnimation (selector) {
            var timeLine = new TimelineMax();
            var mainElement = $(selector).parent().parent();
            var movement = 10;
            timeLine
                .to($(selector).find('.i13m-careers-offer-title'), 0.1, { autoAlpha: 0, display: 'none'})
                .to($(selector).find('.i13m-careers-offer-apply'), 0.1, { autoAlpha: 1, display: 'flex'}, '-=0.1')
                .to([$(selector), $(selector).find('.i13m-careers-offer-apply')], 0.1, { css: { height: '190px' }})
                .to(mainElement, 0.1, { y: '-' + movement, css: { marginTop: '6px', marginBottom: '6px' }},  '-=0.1')
                .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { height: '34px' }}, '-=0.1')
                .to(mainElement.find('.i13m-careers-offer-title.additional'), 0.1, { autoAlpha: 1, display: 'block' }, '-=0.1')
                .to(mainElement.find('.i13m-careers-offer-salary'), 0.1, { autoAlpha: 0, display: 'none' }, '-=0.1')
                .to(mainElement.find('.i13m-careers-offer-city'), 0.1, { css: { color: '#0098da' } }, '-=0.1')
                .to(mainElement.find('.i13m-careers-offer-details'), 0.1, { css: { backgroundColor: '#2c3e50' } }, '-=0.1');
            return timeLine;
        }

        function dispatchKeyEvents (event) {
            var section = _.last(window.location.href.split('/')[3].split('#'));
            var fn = EVENT_FUNCTIONS[section];
            if (!fn) {
                return;
            }
            fn(event);
        }

        function getScreenVariables () {
            sharedVariables.screenHeight = $(window).height();
            sharedVariables.screenWidth = $(window).width();

            if (sharedVariables.screenWidth <= 767) {
                sharedVariables.latestSlides = 1;
                sharedVariables.recruitmentSlides = 2;
            } else if (sharedVariables.screenWidth <= 1024) {
                sharedVariables.latestSlides = 2;
                sharedVariables.recruitmentSlides = 2;
            } else {
                sharedVariables.latestSlides = 4;
            }
        }

        function moveStoryCarouselLeft () {
            if (sharedVariables.successStoriesStoryIndex === 1) {
                return;
            }
            sharedVariables.successStoriesStoryIndex -= 1;
            selectStory();
        }

        function moveStoryCarouselRight () {
            if (sharedVariables.successStoriesStoryIndex >= $('.i13m-success-years-container').children().size()) {
                return;
            }
            sharedVariables.successStoriesStoryIndex += 1;
            selectStory();
        }

        function openStoryControl () {           
            
            var timeLine = new TimelineMax();
            timeLine
                .to($('.i13m-success-control'), 0.6, {
                    css: {
                        rotationY: 360,
                        zIndex: 2
                    }
                })
                .to($('.i13m-careers-itmw-success-content-front'), 0.6, {
                    css: {
                        rotationY: 180,
                        zIndex: 1
                    }
                }, '-=0.6');
        }

        function parseStories () {
            var table = $('.i13m-career-success-table');

            table
                .children()
                .children()
                .each(function () {
                    $(this)
                        .children()
                        .each(function () {
                            var $this = $(this);
                            var person = _.first($this.attr('class').split(' '));
                            var value = $this.html();
                            var year;
                            if (value === '&nbsp;') {
                                return;
                            }
                            if ($this.hasClass('name')) {
                                sharedVariables.successStories[person].name = value;
                                sharedVariables.successStoriesDict[removeDiacritics(value).replace(/\W/gi, '-').toLowerCase()] = person;
                            }
                            if ($this.hasClass('post')) {
                                sharedVariables.successStories[person].post = value;
                            }
                            if ($this.hasClass('year')) {
                                year = $this.attr('class').split(' ')[1];
                                sharedVariables.successStories[person].stories = sharedVariables.successStories[person].stories || { years: {}, texts: {} };
                                sharedVariables.successStories[person].stories.years[year] = value;
                            }
                            if ($this.hasClass('text')) {
                                year = $this.attr('class').split(' ')[1];
                                sharedVariables.successStories[person].stories = sharedVariables.successStories[person].stories || { years: {}, texts: {} };
                                sharedVariables.successStories[person].stories.texts[year] = value;
                            }
                        });
                });
        }

        function removeDiacritics (str) {
            return str.replace(/[^\u0000-\u007E]/g, function(a){
                return diacriticsMap[a] || a;
            });
        }

        function renderStory () {
            var person = window.location.href.split('/')[4];
            var index = person
                ? sharedVariables.successStoriesDict[person]
                    ? sharedVariables.successStoriesDict[person]
                    : sharedVariables.successStoriesIndex
                : sharedVariables.successStoriesIndex;
            var story = sharedVariables.successStories[index];
            var $person = $('.i13m-success-graphic-person');
            var $yearContainer = $('.i13m-success-years-container');
            var $textContainer = $('.i13m-success-text-container');

            $person
                .children('.i13m-success-graphic-name')
                .html(story.name);

            $person
                .children('.i13m-success-graphic-post')
                .html(story.post);

            $yearContainer.empty();
            $textContainer.empty();
            _.each(story.stories.years, function(year, key) {
                var mobileYear = sharedVariables.screenWidth < 768 ? '<p style="width:100%;text-align:center;font-weight:bold;margin:0 0 -.5em;">' + year + '</p><br>' : '';
                $yearContainer.append('<div class="i13m-success-year-container" data-year="' + key + '"><div class="i13m-success-year">' + year + '</div><div class="i13m-success-dot"></div></div>');
                $textContainer.append('<div class="' + key + '">' + mobileYear + story.stories.texts[key] + '</div>');
            });
            $('.i13m-success-year-container').click(selectStory);
            sharedVariables.successStoriesStoryIndex = 1;
            selectStory();
        }

        function renderStoryControl () {
            $('.i13m-success-control')
                .children('.i13m-success-control-person')
                .each(function () {
                    var $this = $(this);
                    var person = _.last($this.attr('class').split(' '));
                    var story = sharedVariables.successStories[person];

                    $this
                        .find('h5')
                        .html(story.name);

                    $this
                        .find('h6')
                        .html(story.post);
                });
        }

        function selectStory () {
            var $this = $(this);
            var yearIndex = $this.data('year') || 'year' + sharedVariables.successStoriesStoryIndex;
            sharedVariables.successStoriesStoryIndex = parseInt(_.last(yearIndex));
            $('.i13m-success-year-container')
                .each(function () {
                    $(this).removeClass('active');
                })
                .filter(function () {
                    return $(this).data('year') === yearIndex;
                })
                .addClass('active');
            $('.i13m-success-text-container')
                .children()
                .removeClass('active')
                .filter(function () {
                    return $(this).attr('class') === yearIndex;
                })
                .addClass('active');
        }

        function setSliderControls (slider, leftSelector, rightSelector) {
            slider.nextSliderThrottled = _.throttle(slider.goToNextSlide, 500, { leading: true, trailing: false });
            slider.prevSliderThrottled = _.throttle(slider.goToPrevSlide, 500, { leading: true, trailing: false });
            $(leftSelector).on('click', function () {
                slider.prevSliderThrottled();
                return false;
            });
            $(rightSelector).on('click', function () {
                slider.nextSliderThrottled();
                return false;
            });
        }

        function setupH2Animation () {
            var timeLine = new TimelineMax({
                onComplete: function() {
                    this.restart();
                }
            });
            var textDelay = 8;
            $('.i13m-careers-search-content')
                .children('h2')
                .children('p')
                .sort(function () {
                    return Math.random() > 0.5 ? 1 : -1;
                })
                .each(function () {
                    timeLine
                        .to($(this), 0.3, { autoAlpha: 1, display: 'inline' })
                        .to($(this), 0.3, { autoAlpha: 0, display: 'none' }, '+=' + textDelay);
                });
        }

        function setupStories () {
            parseStories();
            renderStoryControl();
            renderStory();
            setupStoryWatchers();
            openStoryControl();
        }

        function setupStoryWatchers() {
            $('.i13m-success-control-person').click(function () {
                var $this = $(this);
                var person = _.last($this.attr('class').split(' '));
                sharedVariables.successStoriesIndex = person;
                renderStory();
                closeStoryControl();
            });

            $('.i13m-success-control-close').click(closeStoryControl);
            $('.i13m-success-more').click(openStoryControl);
            $('.i13m-left-button.success').click(moveStoryCarouselLeft);
            $('.i13m-right-button.success').click(moveStoryCarouselRight);
        }

        function setHoverWatchers () {

            $('.i13m-careers-offer-link')
                .each(function () {
                    $(this).hover(function () {
                        if (!this.animation) {
                            this.animation = createAnimation(this);
                        } else {
                            this.animation.play().timeScale(1);
                        }
                    }, function () {
                        if (!this.animation) {
                            this.animation = createAnimation(this);
                        } else {
                            this.animation.reverse().timeScale(4);
                        }
                    });
                });
        }

        function showSliderOffers () {
            $('.bxslider')
                .find('.i13m-careers-offer')
                .css({ display: 'block'});
        }           
        function recruitmentAnimation() {
            var timeLine = new TimelineMax();
            timeLine
            .to($('.i13m-careers-recruitment-text-title'), 0.3, { autoAlpha: 1 })
            .to($('.i13m-careers-recruitment-text-subtitle'), 0.3, { autoAlpha: 1 });

            $('.i13m-careers-recruitment-animation-elements')
            .children()
            .each(function () {
                timeLine.to($(this), 0.1, { autoAlpha: 1 }, '-=0.05');
            });

            $('.i13m-careers-recruitment-animation-texts')
            .children('td[colspan="3"]')
            .each(function () {
                timeLine.to($(this), 0.2, { autoAlpha: 1 }, '-=0.2');
            });

            timeLine.to($('.i13m-careers-recruitment-footer'), 0.3, { autoAlpha: 1 });
        }  
        
        function  referralLoad() {
        var timeLine = new TimelineMax();
        var $container = $('.i13m-careers-itmw-referral-widget');
        var $text = $container.find('.i13m-text');

        timeLine
          .to($text, 0.3, { autoAlpha: 1})
          .to($('.i13m-careers-referral-story1'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-careers-referral-story2'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-careers-referral-story3'), 0.3, { autoAlpha: 1 }, '-=0.1')
          .to($('.i13m-referral-faq-link'), 0.3, { autoAlpha: 1 }, '-=0.1');
      }          
        
    function latestJobsSearch1(){ 

        const input = document.querySelector('#latest-jobs-searcher');
        const all = document.querySelectorAll('.i13m-careers-latest-carousel .i13m-job-post');       
        let   list = document.querySelector('.i13m-careers-latest-carousel .bxslider');       
        let standarize = (val) => val.toLowerCase();

        function search(e) {                         
            if(e.keyCode == 13 || e.keyCode == 8) {                       
                list.innerHTML = '';
                let phrase = standarize(this.value);
                let filtered = [...all].filter((item)=>{                                                 
                    standarize(item.innerText).indexOf(phrase) !== -1;
                });                                     
                    console.log('fitlered ' + filtered);         
                filtered.forEach((li)=>{                                     
                    list.append(li);
                    console.log('lista ' + list);
                }); 
                plugins.latestSlider.goToSlide(1);
            }
        }
    input.addEventListener('keyup',search);     
    }    
      
      
    function latestJobsSearch(){            

            $('#latest-jobs-searcher').keyup(function() {
                
                $('.no_results-latest').hide();
                $('.i13m-left-button.latest,.i13m-right-button.latest').show();

                if(event.keyCode == 13 || event.keyCode == 8){                     
                    showSliderOffers();               
                    var phrase = $(this).val().toLowerCase().replace(/\s/g, '');                                                                             
                    $('.latest-offer .i13m-careers-offer-title').each(function () {
                        var name = $(this).text().toLowerCase().replace(/\s/g, '');
                        
                        (name.indexOf(phrase) !== -1) 
                        ? $(this).closest('.i13m-job-post').show()                     
                        : $(this).closest('.i13m-job-post').hide();                        
                    });
                    plugins.latestSlider.goToSlide(1);                      

                    if($('.latest-offer:visible').length == 0) {                       
                        $('.no_results-latest').show();
                        $('.i13m-left-button.latest,.i13m-right-button.latest').hide();
                    };
                   
                }
            });
        }
        function privacyPolicy() {               
            
            var closeModal = ()=> $('.modal-privacy').hide(),            
                openModal = () => $('.modal-privacy').show(),
            
                turnOffTrigger = function() {
                $('.privacy-trigger')
                    .off('click')
                    .css({'cursor':'text','text-decoration':'none'})
                    .text('You agreed to the privacy policy')
                },
         
                checkInputs = function(id){ 
                $(id).change(function(){                    
                    var agree = $('input[name=newsletter_ok]');
                        if(privacy1.is(':checked') && privacy2.is(':checked') && privacy3.is(':checked') ) {
                            agree.prop('checked',true);
                            closeModal();
                            turnOffTrigger();                        
                        }
                    });                
                },     
                privacy1 = $('#privacy1'),
                privacy2 = $('#privacy2'),
                privacy3 = $('#privacy3'),
                inputs = $('.modal-privacy input');
            
            $('.modal-privacy-close').click(closeModal);
            $('.privacy-trigger').click(openModal);         
            [...inputs].forEach(function(item){
                checkInputs("#"+item.getAttribute('id'));               
            });          
        }

        $(document).ready(function () {                
            getScreenVariables();
            initPlugins();
            setHoverWatchers();
            showSliderOffers();
            setupH2Animation();
            setupStories();
            addSliderResizer();
            addKeyEventWatchers();                  
            latestJobsSearch(); 
            privacyPolicy();
        });       

    }(jQuery));
}