# Proces instalacji

1. Zainstalować node.js (https://nodejs.org/en/) w wersji co najmniej 6
2. Zainstalować git (https://git-scm.com/)
3. Sklonować oba repozytoria w jednym katalogu (dowolnym):
   1. https://bitbucket.org/itmagination_website/itmagination.com
   2. https://bitbucket.org/itmagination_website/itmagination_bt.com
4. Zainstalować bower: `npm install -g bower`
5. W katalogu `itmagination_bt`:
   1. Zainstalować zależności za pomocą polecenia `npm install`
   2. Dodać pakiet do zmiennej globalne za pomocą polecenia `npm link`
6. W katalogu `itmagination`:
   1. Zainstalować zależności za pomocą polecenia `bower install`

# Podstawowe komendy build toola

* `server` - _lokalny serwer deweloperski, nieużywany od porzucenia wersji w angularze_
* `build` - kompilacja plików .js i .css
   * `--upload, -u` - po kompilacji pliki zostaną przesłane na ftp na HubSpocie
   * `--skip, -s` - pomija kompilację .css (niestabilne)
   * `--css, -c` - buduje oddzielne pliki .css na podstawie manifestów poszczególnych podstron (wymaga zmian po stronie HubSpota)
   * `--help, -h` - pomoc

# Co jest potrzebne do utworzenia nowej podstrony?

1. Utworzenie w `Website Pages` w HubSpocie nowej strony (w przypadku podstrony do `Careers` - w `Landing Pages`) - najłatwiejszym sposobem jest sklonowanie istniejącej podstrony
2. Nadanie podstronie właściwego URL w formacie `{domena}/{język}/{podstrona}/{opcjonalnie:podpodstrona}`
3. Przypisanie do podstrony właściwego template'u - najłatwiejszym sposobem jest sklonowanie istniejącego template'u
4. Template powinien składać się następujących modułów:
   1. W przypadku podstron korzystających z `fullPage.js` (one-page scroll) w domenie `www`:
      * `ITMAGINATION_header`
      * moduł zawierający cały content podstrony wraz z `ITMAGINATION_footer` w ostatniej sekcji
      * `ITMAGINATION_additional_JS`
   2. W przypadku podstron korzystających z `fullPage.js` w domenie `careers` zamiast `ITMAGINATION_header` należy użyć `ITMAGINATION_Careers_header`
   3. W przypadku podstron niekorzystających z `fullPage.js`:
      * `ITMAGINATION_header_absolute`
      * moduł zawierający cały content
      * `ITMAGINATION_additional_JS`
      * `ITMAGINATION_footer`
5. Style do danej podstrony powinny znaleźć się w repozytorium głównym, w pliku `app\components\styling\style\{podstrona}.less`. Plik powinien zostać zaimportowany do pliku `app\components\styling\style\index.less`
6. Należy dodać do katalogu `app\components` katalogu `{podstrona}` zawierającego pliki:
   * `i13m.json` - w którym powinny znajdować się następujące sekcje:
      * `name` zawierająca wartość: `{podstrona}`
      * `coreJs` zawierająca pliki:
         * `bower_components/store-js/store.js`
         * `bower_components/gsap/src/uncompressed/plugins/CSSPlugin.js`
         * `bower_components/gsap/src/uncompressed/TimelineMax.js`
         * `bower_components/gsap/src/uncompressed/TweenMax.js`
         * `bower_components/gsap/src/uncompressed/easing/EasePack.js`
         * `bower_components/lodash/dist/lodash.js`
         * `bower_components/bxslider-4/dist/jquery.bxslider.js`
         * `app/components/shared/index.js`
         * `app/components/shared/jquery.fullPage.js`
         * `app/components/shared/hotjar.js`
         * `app/components/shared/ga.js`
         * `app/components/{podstrona}/index.js`
         * pozostałe pliki, w których funkcjonalność została zaimplementowana
   * `index.js` - entry point każdego komponentu
   * pozostałe pliki implementujące funkcjonalność, wywowływane w `index.js`
7. W module `ITMAGINATION_additional_JS` należy dodać wartość `{podstrona}` do listy w linii **8** (jeżeli jest to zwykła podstrona), **12** (jeżeli jest to podstrona testowa) lub w uzasadnionych przypadkach - do osobnej listy (por lista dotycząca `rebrandingtour`). W przypadku podstrony w domenie `careers` należy rozgałęzić wyrażenie warunkowe dotyczące tej podstrony.

# Charakterystyka istniejących podstron
